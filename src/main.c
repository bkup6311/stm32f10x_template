
#include "main.h"
#include "datetime.h"
#include "bitband.h"
#include "ds3231.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if 0
void rtc_set_alarm(uint32_t);
void rtc_set_alarm(uint32_t alarm) {

  //uint32_t apb1flags = 0;
  //if ((RCC->APB1ENR & RCC_APB1ENR_BKPEN) != RCC_APB1ENR_BKPEN) {
  //  RCC->APB1ENR |= RCC_APB1ENR_BKPEN;
  //  apb1flags |= RCC_APB1ENR_BKPEN;
  //}
  //
  //if ((RCC->APB1ENR & RCC_APB1ENR_PWREN) != RCC_APB1ENR_PWREN) {
  //  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  //  apb1flags |= RCC_APB1ENR_PWREN;
  //}
  //

  uint32_t timeout = 1000;
  while ((RTC->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET) {
    /* just wait for RTOFF  */
    timeout -= GET_TICK();
    if (timeout == 0) return;
  }

  while ((RTC->CRL & RTC_CRL_SECF) == (uint32_t)RESET) {
    /* just wait for RTOFF  */
    // timeout -= GET_TICK();
    // if (timeout == 0) return;
  }

  //PWR->CR |= PWR_CR_DBP;
  RTC->CRL |= RTC_CRL_CNF;

  /* Set RTC COUNTER MSB word */
  RTC->ALRH = alarm >> 16;
  /* Set RTC COUNTER LSB word */
  RTC->ALRL = alarm;

  RTC->CRL &= ~RTC_CRL_CNF;
  // PWR->CR &= ~PWR_CR_DBP;

  timeout = 1000;
  while (((RTC->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET) && (timeout -= GET_TICK())) {
    /* just wait for RTOFF  */
  }

  // RCC->APB1ENR &= ~apb1flags;
}
#else
void rtc_set_alarm(uint32_t);
void rtc_set_alarm(uint32_t alarm_time) {

  #if 0
  while((RTC->CRL & RTC_CRL_SECF) != RTC_CRL_SECF)  {
      /* JUST WAIT */
  }
  RTC->CRL &= (uint16_t)~(RTC_CRL_ALRF | RTC_CRL_SECF);

  uint32_t timeout = 1000;
  while ((RTC->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET) {
    /* just wait for RTOFF  */
    timeout -= GET_TICK();
    if (timeout == 0) return;
  }

  uint32_t apb1enr_flags = 0;
  if ((RCC->APB1ENR & RCC_APB1ENR_BKPEN) != RCC_APB1ENR_BKPEN) {
    RCC->APB1ENR |= RCC_APB1ENR_BKPEN;
    apb1enr_flags |= RCC_APB1ENR_BKPEN;
  }

  if ((RCC->APB1ENR & RCC_APB1ENR_PWREN) != RCC_APB1ENR_PWREN) {
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    apb1enr_flags |= RCC_APB1ENR_PWREN;
  }
  #endif


  RCC->APB1ENR |= (RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN);

  // while((RTC->CRL & RTC_CRL_RSF) != RTC_CRL_RSF)  {
  //     /* JUST WAIT */
  // }

  #if 0
  while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF) {
      /* JUST WAIT */
  }
  #endif

  PWR->CR |= PWR_CR_DBP;                      /* Disable Backup Domain Protection                                                                                   */

  uint32_t timeout = 1000;
  while ((RTC->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET) {
    /* just wait for RTOFF  */
    timeout -= GET_TICK();
    if (timeout == 0) return;
  }

  RTC->CRL |= RTC_CRL_CNF;

  alarm_time--;

  RTC->ALRH = alarm_time >> 16;
  RTC->ALRL = alarm_time;

  RTC->CRL &= (uint16_t)~(RTC_CRL_ALRF | RTC_CRL_SECF);
  RTC->CRH |= RTC_CRH_ALRIE;
  RTC->CRL = ~RTC_CRL_CNF;

  RTC->CRL &= (uint16_t)~RTC_CRL_RSF;
  while((RTC->CRL & RTC_CRL_RSF) != RTC_CRL_RSF) {}

  // RCC->APB1ENR &= ~apb1enr_flags;

  // while((RTC->CRL & RTC_CRL_RSF) != RTC_CRL_RSF)  {
  //     /* JUST WAIT */
  // }


  timeout = 1000;
  while ((RTC->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET) {
    /* just wait for RTOFF  */
    timeout -= GET_TICK();
    if (timeout == 0) return;
  }

  PWR->CR &= ~PWR_CR_DBP;                     /* Restore Backup Domain Protection                                                                                   */
  RCC->APB1ENR &= ~(RCC_APB1ENR_PWREN | RCC_APB1ENR_BKPEN);
}
#endif
 extern volatile char alarm_flag;
 volatile char alarm_flag = ' ';

// static void to_hex(char *buf, uint64_t d) {
//   unsigned ndx = 0;
//   for(unsigned i = 0; i < sizeof(uint64_t); i++) {
//     char c = d >> (i * 8) & 0xFF;
//     buf[ndx++] = TO_H(c >> 4);
//     buf[ndx++] = TO_H(c & 0x0F);
//   }
// }

static char out_buf[255];
//static uint32_t out_data;

extern volatile unsigned randomizer, t_cnt, s_cnt, tick_period, freq[20];
volatile unsigned randomizer, t_cnt = 0, s_cnt = 0, tick_period = 0, freq[20] = {0};

extern volatile unsigned short measured_period;
volatile unsigned short measured_period = 0;
enum {O_SIZE = 16};
static uint32_t o_data[O_SIZE];
static unsigned o_ndx;

#if USE_I2C
  unsigned short i2c_status = 0;
#endif

#if USE_UART
  static const char * const python_script = "#!/usr/bin/env python\n\ndef main():\n\tbin_file = open('out_data.bin', 'wb')"
                                             "\n\tbin_file.write(bytearray(bytes.fromhex(DATA)))\n\tbin_file.close()\n\nDATA = '''\n";

  static const char * const python_script_tail = "\nif __name__ == '__main__':\n\tmain()\n";
#endif

__STATIC_INLINE void hex_string(char * buf, char * data, uint32_t len) {
  unsigned k = 0;
  for(unsigned j = 0; j < len; j++) {
    buf[k++] = HEX(data[j] >> 4);
    buf[k++] = HEX(data[j] & 0x0F);
  }
  buf[k++] = '\n';
  buf[k] = 0;
}

#define TOTAL_LINES 650000
#define BYTES_PER_LINE 64

__STATIC_INLINE void configure_clock(uint32_t pll_mul) {

  RCC->CFGR = RCC_CFGR_SW_HSI;
  while ((RCC->CFGR & RCC_CFGR_SWS_HSI) != RCC_CFGR_SWS_HSI) {
    /* JUST WAIT */
  }

  // RCC->CR &= ~RCC_CR_PLLON;
  RCC->CR = RCC_CR_HSEON | RCC_CR_HSION; /*  Switch PLL off, HSE and HSI on                                                                                    */
  while(RCC->CR & RCC_CR_PLLRDY) {}      /*  Wait for PLL clock ready flag                                                                                     */
  while(!(RCC->CR & RCC_CR_HSERDY)) {}   /*  Wait until HSE becomes stable                                                                                     */

  RCC->CFGR = (
    1 * RCC_CFGR_PLLSRC                | /*  HSE clock selected as PLL entry clock source                                                                      */
    #if 0
      1 * RCC_CFGR_PLLMULL7              /*  PLL input clock * 7                                                                                               */
    #else
      pll_mul
    #endif
  );
  RCC->CR |= RCC_CR_PLLON;
  while(!(RCC->CR & RCC_CR_PLLRDY)) {}   /*  Wait for PLL clock ready flag                                                                                     */

  RCC->CFGR = (
    0 * RCC_CFGR_SW                     | /*  0x00000003 SW[1:0] bits (System clock Switch)                                                                     */
    0 * RCC_CFGR_SW_0                   | /*    0x00000001                                                                                                      */
    0 * RCC_CFGR_SW_1                   | /*    0x00000002                                                                                                      */
    0 * RCC_CFGR_SW_HSI                 | /*    HSI selected as system clock                                                                                    */
    0 * RCC_CFGR_SW_HSE                 | /*    HSE selected as system clock                                                                                    */
    1 * RCC_CFGR_SW_PLL                 | /*    PLL selected as system clock                                                                                    */

    0 * RCC_CFGR_SWS                    | /*  0x0000000C SWS[1:0] bits (System Clock Switch Status)                                                             */
    0 * RCC_CFGR_SWS_0                  | /*    0x00000004                                                                                                      */
    0 * RCC_CFGR_SWS_1                  | /*    0x00000008                                                                                                      */
    0 * RCC_CFGR_SWS_HSI                | /*    HSI oscillator used as system clock                                                                             */
    0 * RCC_CFGR_SWS_HSE                | /*    HSE oscillator used as system clock                                                                             */
    0 * RCC_CFGR_SWS_PLL                | /*    PLL used as system clock                                                                                        */

    0 * RCC_CFGR_HPRE                   | /*  0x000000F0 HPRE[3:0] bits (AHB prescaler)                                                                         */
    0 * RCC_CFGR_HPRE_0                 | /*    0x00000010                                                                                                      */
    0 * RCC_CFGR_HPRE_1                 | /*    0x00000020                                                                                                      */
    0 * RCC_CFGR_HPRE_2                 | /*    0x00000040                                                                                                      */
    0 * RCC_CFGR_HPRE_3                 | /*    0x00000080                                                                                                      */
    0 * RCC_CFGR_HPRE_DIV1              | /*    SYSCLK not divided                                                                                              */
    0 * RCC_CFGR_HPRE_DIV2              | /*    SYSCLK divided by 2                                                                                             */
    0 * RCC_CFGR_HPRE_DIV4              | /*    SYSCLK divided by 4                                                                                             */
    0 * RCC_CFGR_HPRE_DIV8              | /*    SYSCLK divided by 8                                                                                             */
    0 * RCC_CFGR_HPRE_DIV16             | /*    SYSCLK divided by 16                                                                                            */
    0 * RCC_CFGR_HPRE_DIV64             | /*    SYSCLK divided by 64                                                                                            */
    0 * RCC_CFGR_HPRE_DIV128            | /*    SYSCLK divided by 128                                                                                           */
    0 * RCC_CFGR_HPRE_DIV256            | /*    SYSCLK divided by 256                                                                                           */
    0 * RCC_CFGR_HPRE_DIV512            | /*    SYSCLK divided by 512                                                                                           */

    0 * RCC_CFGR_PPRE1                  | /*  0x00000700 PRE1[2:0] bits (APB1 prescaler)                                                                        */
    0 * RCC_CFGR_PPRE1_0                | /*    0x00000100                                                                                                      */
    0 * RCC_CFGR_PPRE1_1                | /*    0x00000200                                                                                                      */
    0 * RCC_CFGR_PPRE1_2                | /*    0x00000400                                                                                                      */
    0 * RCC_CFGR_PPRE1_DIV1             | /*    HCLK not divided                                                                                                */
    1 * RCC_CFGR_PPRE1_DIV2             | /*    HCLK divided by 2                                                                                               */
    0 * RCC_CFGR_PPRE1_DIV4             | /*    HCLK divided by 4                                                                                               */
    0 * RCC_CFGR_PPRE1_DIV8             | /*    HCLK divided by 8                                                                                               */
    0 * RCC_CFGR_PPRE1_DIV16            | /*    HCLK divided by 16                                                                                              */

    0 * RCC_CFGR_PPRE2                  | /*  0x00003800 PRE2[2:0] bits (APB2 prescaler)                                                                        */
    0 * RCC_CFGR_PPRE2_0                | /*    0x00000800                                                                                                      */
    0 * RCC_CFGR_PPRE2_1                | /*    0x00001000                                                                                                      */
    0 * RCC_CFGR_PPRE2_2                | /*    0x00002000                                                                                                      */
    0 * RCC_CFGR_PPRE2_DIV1             | /*    HCLK not divided                                                                                                */
    0 * RCC_CFGR_PPRE2_DIV2             | /*    HCLK divided by 2                                                                                               */
    0 * RCC_CFGR_PPRE2_DIV4             | /*    HCLK divided by 4                                                                                               */
    0 * RCC_CFGR_PPRE2_DIV8             | /*    HCLK divided by 8                                                                                               */
    0 * RCC_CFGR_PPRE2_DIV16            | /*    HCLK divided by 16                                                                                              */

    #if USE_ADC
      0 * RCC_CFGR_ADCPRE               | /*  0x0000C000 ADCPRE[1:0] bits (ADC prescaler)                                                                       */
      0 * RCC_CFGR_ADCPRE_0             | /*    0x00004000                                                                                                      */
      0 * RCC_CFGR_ADCPRE_1             | /*    0x00008000                                                                                                      */
      0 * RCC_CFGR_ADCPRE_DIV2          | /*    PCLK2 divided by 2                                                                                              */
      1 * RCC_CFGR_ADCPRE_DIV4          | /*    PCLK2 divided by 4                                                                                              */
      0 * RCC_CFGR_ADCPRE_DIV6          | /*    PCLK2 divided by 6                                                                                              */
      0 * RCC_CFGR_ADCPRE_DIV8          | /*    PCLK2 divided by 8                                                                                              */
    #endif /* USE_ADC */

    0 * RCC_CFGR_PLLSRC                 | /*  0x00010000 PLL entry clock source.                          This bit can be written only when PLL is disabled.    */
    0 * RCC_CFGR_PLLXTPRE               | /*  0x00020000 HSE divider for PLL entry.                       This bit can be written only when PLL is disabled.    */
    0 * RCC_CFGR_PLLXTPRE_HSE           | /*    HSE clock not divided for PLL entry                                                                             */
    0 * RCC_CFGR_PLLXTPRE_HSE_DIV2      | /*    HSE clock divided by 2 for PLL entry                                                                            */

    #if 0
      0 * RCC_CFGR_PLLMULL              | /*  0x003C0000 PLLMUL[3:0] bits (PLL multiplication factor)     These bits can be written only when PLL is disabled.  */
      0 * RCC_CFGR_PLLMULL_0            | /*    0x00040000                                                                                                      */
      0 * RCC_CFGR_PLLMULL_1            | /*    0x00080000                                                                                                      */
      0 * RCC_CFGR_PLLMULL_2            | /*    0x00100000                                                                                                      */
      0 * RCC_CFGR_PLLMULL_3            | /*    0x00200000                                                                                                      */
      0 * RCC_CFGR_PLLMULL2             | /*    0x00000000 PLL input clock * 2                                                                                  */
      0 * RCC_CFGR_PLLMULL3             | /*    0x00040000 PLL input clock * 3                                                                                  */
      0 * RCC_CFGR_PLLMULL4             | /*    0x00080000 PLL input clock * 4                                                                                  */
      0 * RCC_CFGR_PLLMULL5             | /*    0x000C0000 PLL input clock * 5                                                                                  */
      0 * RCC_CFGR_PLLMULL6             | /*    0x00100000 PLL input clock * 6                                                                                  */
      0 * RCC_CFGR_PLLMULL7             | /*    0x00140000 PLL input clock * 7                                                                                  */
      0 * RCC_CFGR_PLLMULL8             | /*    0x00180000 PLL input clock * 8                                                                                  */
      0 * RCC_CFGR_PLLMULL9             | /*    0x001C0000 PLL input clock * 9                                                                                  */
      0 * RCC_CFGR_PLLMULL10            | /*    0x00200000 PLL input clock * 10                                                                                 */
      0 * RCC_CFGR_PLLMULL11            | /*    0x00240000 PLL input clock * 11                                                                                 */
      0 * RCC_CFGR_PLLMULL12            | /*    0x00280000 PLL input clock * 12                                                                                 */
      0 * RCC_CFGR_PLLMULL13            | /*    0x002C0000 PLL input clock * 13                                                                                 */
      0 * RCC_CFGR_PLLMULL14            | /*    0x00300000 PLL input clock * 14                                                                                 */
      0 * RCC_CFGR_PLLMULL15            | /*    0x00340000 PLL input clock * 15                                                                                 */
      0 * RCC_CFGR_PLLMULL16            | /*    0x00380000 PLL input clock * 16                                                                                 */
    #endif

    0 * RCC_CFGR_USBPRE                 | /*  0x00400000 USB Device prescaler                                                                                   */

    0 * RCC_CFGR_MCO                    | /*  0x07000000 MCO[2:0] bits (Microcontroller Clock Output)                                                           */
    0 * RCC_CFGR_MCO_0                  | /*    0x01000000                                                                                                      */
    0 * RCC_CFGR_MCO_1                  | /*    0x02000000                                                                                                      */
    0 * RCC_CFGR_MCO_2                  | /*    0x04000000                                                                                                      */
    0 * RCC_CFGR_MCO_NOCLOCK            | /*    No clock                                                                                                        */
    0 * RCC_CFGR_MCO_SYSCLK             | /*    System clock selected as MCO source                                                                             */
    0 * RCC_CFGR_MCO_HSI                | /*    HSI clock selected as MCO source                                                                                */
    0 * RCC_CFGR_MCO_HSE                | /*    HSE clock selected as MCO source                                                                                */
    0 * RCC_CFGR_MCO_PLLCLK_DIV2          /*    PLL clock divided by 2 selected as MCO source                                                                   */
  );

  while (!(RCC->CFGR & RCC_CFGR_SWS_PLL)) {}

  SystemCoreClockUpdate();

  #if 1
    SysTick->LOAD = AHB_CLK / (8 KHZ) - 1;    /* set reload register T = 1 ms                                                                                       */
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;  /* start SysTick timer                                                                                                */
  #else
    SysTick->LOAD = AHB_CLK / (1 KHZ) - 1;    /* T = 1 ms                                                                                                           */
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
  #endif

  #if 0
  RCC->CFGR = (
    1 * RCC_CFGR_SW_PLL                 | /*    PLL selected as system clock                                                                                    */
    1 * RCC_CFGR_PPRE1_DIV2             | /*    HCLK divided by 2                                                                                               */
    1 * RCC_CFGR_ADCPRE_DIV6              /*    PCLK2 divided by 6                                                                                              */
  );
  #endif
}

#if USE_BTN_IT
  extern volatile unsigned key_pressed;
  volatile unsigned key_pressed = 0;
#endif

int main(void) {

  // uint8_t *uid = (uint8_t*)UID_BASE;

  sysInit();
  
  (void)&configure_clock/*(RCC_CFGR_PLLMULL14)*/;
  //USART_PORT->BRR = UART_BAUDRATE(APB1_CLK, USE_UART_SPEED);
  char *s;

  if (RCC->CFGR & RCC_CFGR_SWS_PLL) {
    #if defined(USE_HSE) && USE_HSE
      s = "PLL (HSE)";
    #else
      s = "PLL (HSI)";
    #endif
  } else if (RCC->CFGR & RCC_CFGR_SWS_HSE) {
    s = "HSE";
  } else {
    s = "HSI";
  }

  #if defined(__GNUC__) && !defined(__clang__)
    uprintf("System core clock: %lu Hz, %s.\n", SystemCoreClock, s);
  #else
    uprintf("System core clock: %u Hz, %s.\n", SystemCoreClock, s);
    uprintf("System bore clock: %u Hz, %s.\n", SystemCoreClock, s);
    uprintf("System lore clock: %u Hz, %s.\n", SystemCoreClock, s);
    uprintf("System more clock: %u Hz, %s.\n", SystemCoreClock, s);
    uprintf("System zore clock: %u Hz, %s.\n", SystemCoreClock, s);
  #endif

  TIM4->PSC = AHB_CLK / (1 MHZ) - 1;
  //TIM4->ARR = 10000 - 1;
  TIM4->EGR = TIM_EGR_UG;
  TIM4->SR = 0;
  //TIM4->DIER = TIM_DIER_UIE;
  TIM4->CR1 = TIM_CR1_CEN;

  // uprintf("Just started.\n");
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;

  #if 0
  enum {CURRENT = 0, PREVIOUS = 1, TOTAL = 2};
  static unsigned key_state[TOTAL];

  while(TIM1->CR1 != 751) {

    if ((GET_TICK() && !(key_state[CURRENT] = (key_state[CURRENT] << 1) | READ_PIN(B, 12)) && key_state[PREVIOUS])) {
      u_puts("key pressed\n");
    }
    key_state[PREVIOUS] = key_state[CURRENT];
  #else

  #if USE_I2C
    #if 0
      ds3231_control_t ctrl = DS3231_CONTROL_DATA;
      ds3231_status_t status = DS3231_STATUS_DATA;
    #else
      char ctrl[2] = {0x0E, (1 << 3) | (0 << 4)};
      char status[2] = {0x0F, 1 << 3};
    #endif

    i2c_write(DS3231_I2C_ADDR, (char*)&ctrl, sizeof(ctrl));
    i2c_write(DS3231_I2C_ADDR, (char*)&status, sizeof(status));

    #if 1
      char wr[12] = {0x00, 0x00, 0xA1, 0xB2, 0xC3, 0xD4, 0xE5, 0xF6, 0xA7, 0xB8, 0xC9, 0xDA};
    #else
      char wr[12] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    #endif
    i2c_write(AT24C32_I2C_ADDR, (char*)&wr, sizeof(wr));

    DELAY_MS(100);

    char rd[10] = {0};
    i2c_write(AT24C32_I2C_ADDR, "\x00\x00", 2);
    i2c_read(AT24C32_I2C_ADDR, (char*)&rd, sizeof(rd));
    __NOP();

  #endif


  unsigned tcnt = 0, rgb = !0;
  union {
    char r[6];
    unsigned short addr;
  } rnd = {.addr = 0};


  while(TIM1->CR1 != 751) {
  #endif
    #if USE_BTN_IT != 0
      if (key_pressed != 0) {
        key_pressed = 0;
        u_puts("key pressed\n");
      }
    #else
      // if (GET_TICK() && (++tcnt == 2)) {
      if (GET_TICK() && (++tcnt == 2)) {
        tcnt = 0;
        if (is_key_pressed(READ_PIN(B, 12))) {
          #if USE_UART
            u_puts("key pressed\n");
            for(unsigned i = 0; i < 20; i++) {
              uprintf("%u -- %u\n", 3410 + i, freq[i]);
            }
          #endif
        }
      }
    #endif

    #if 0
    if (RTC->CRL & RTC_CRL_SECF) {
      t_cnt = s_cnt;
      GPIOC->BSRR = GPIO_BSRR_BR13;
      s_cnt = 0;
      RTC->CRL &= ~RTC_CRL_SECF;
    #else
    if (!(GPIOC->ODR & GPIO_ODR_ODR13)) {
    #endif
      //for(unsigned i = 0; i < 96 / 8; i++) {
      //  u_putx(uid[i]);
      //}
      //
      //CRC->DR = (uint32_t) rand();

      time_struct_t t;
      unsigned curr_time = rtc_read_counter();
      unixtime_to_time(curr_time, &t);

      #if USE_I2C
        union {
          unsigned r;
          char c[sizeof(unsigned)];
        } x1;

        do {
          x1.r = randomizer;
        } while (x1.r != randomizer);

        for(unsigned i = 0; i < sizeof(unsigned); i++) {
          rnd.r[i + 2] = x1.c[i];
        }
        i2c_write(AT24C32_I2C_ADDR, rnd.r, sizeof(rnd.r));

        unsigned x2 = __REV16(rnd.addr);

        if (++x2 == 1024) {
          rnd.addr = 0;
        } else {
          rnd.addr = (unsigned short) __REV16(x2);
        }

        union {
          char tdata[sizeof(ds3231_register_pool_t)];
          ds3231_register_pool_t ds3231;
        } u;
        i2c_read_reg(DS3231_I2C_ADDR, u.tdata, 0, sizeof(ds3231_register_pool_t));
        //tdata[5] &= ~(1 << 7);
      #endif

      #if 0
      uprintf("%02u.%02u.%02u %02u:%02u:%02u  %ums, %uus\n", t.day, t.month, t.year, t.hour, t.minute, t.second, t_cnt, tick_period);
      #else

      unsigned long long x = measured_period;
      #if 0
        x = x * 3276806 / 1400;
      #endif

      uprintf("%02u.%02u.%02u %02u:%02u:%02u (%02u.%02u.%02u %02u:%02u:%02u)  %ums, %uus, %02X, %02X, %u, %08X\n", t.day, t.month, t.year, t.hour, t.minute, t.second,
              BCD2DEC(u.ds3231.date.d), BCD2DEC(u.ds3231.month.m), BCD2DEC(u.ds3231.year.y), BCD2DEC(u.ds3231.hour.h), BCD2DEC(u.ds3231.min.m), BCD2DEC(u.ds3231.sec.s), t_cnt, tick_period, u.tdata[0x0E], u.tdata[0x0F], (unsigned)x, randomizer);
      #endif

      #if 0
        rgb <<= 1;
        if (rgb == (1 << 3)) {
          rgb = !0;
        }
        GPIOA->BSRR = 0x00070000 | rgb;
      #else
        unsigned ndx = measured_period - 3410;
        if (ndx == 8) {
          rgb = 2;
        } else if (ndx < 8) {
          rgb = 1;
        } else {
          rgb = 4;
        }
        GPIOA->BSRR = 0x00070000 | rgb;
      #endif

      GPIOC->BSRR = GPIO_BSRR_BS13;

      #if !0
        _SW(B, 5, LOW);
        _SW(B, 4, HIGH);
      #endif

      DELAY_MS(2);

      #if !0
        _SW(B, 4, LOW);
      #endif

      GPIOA->BSRR = 0x00070000;

      #if USE_IWDG
        IWDG->KR = 0xAAAA;
      #endif

    }
    // if (GET_TICK()) {
    //   TIM4->CR1 = 0;
    //   tick_period = TIM4->CNT;
    //   TIM4->EGR = TIM_EGR_UG;
    //   TIM4->SR = 0;
    //   TIM4->CR1 = TIM_CR1_CEN;
    //   s_cnt ++;
    // }
  }

  #if USE_UART
    u_puts(python_script);
  #endif

  //ADC1->SMPR1 = ADC_SMPR1(16, 239) | ADC_SMPR1(17, 239);
  ADC1->SMPR1 = ADC_SMPR1(16, 239) | ADC_SMPR1(17, 239);
  ADC1->SQR3 = ADC_SQR3(1, 16);
  ADC1->SQR1 = ADC_SQR1_LEN(1);

  ADC1->CR2 |= ADC_CR2_ADON;
  while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
    /* Wait until conversion completes */
  }

  (void)ADC1->DR;

  ADC1->CR2 |= ADC_CR2_CONT;
  ADC1->CR2 |= ADC_CR2_ADON;

  // while ((RTC->CRL & RTC_CRL_SECF) != RTC_CRL_SECF) {
  //   /* wait */
  // }
  // rtc_set_alarm(rtc_read_counter() + 15);
  // RTC->CRL &= ~RTC_CRL_SECF;

  // rtc_set_alarm(rtc_read_counter() + 10);
  #if 1
  uint32_t rbit[4], ndx = 0, /* out_buf_ndx = 0, */ rb_count = 0;
  // uint64_t out_data[8];

  // union {
  //   uint64_t random;
  //   char rnd[sizeof(uint64_t)];
  // } random_bits; /* = {0}; */
  #endif

  // union {
  //   char c[200];
  //   uint16_t w[100 / 2];
  // } w = {.w[0] = TO_HEX(0x12), .w[1] = TO_HEX(0x23), .w[2] = TO_HEX(0x34),
  //        .w[3] = TO_HEX(0x45), .w[4] = TO_HEX(0x56), .w[5] = TO_HEX(0x67),
  //        .w[6] = TO_HEX(0x78), .w[7] = TO_HEX(0x89), .w[8] = TO_HEX(0x9A)};
  //
  // if ((volatile char)w.c[0] == 0) {
  //   __NOP();
  // }
  //

  uint32_t c = 0, line_count = 0;

  TIM4->PSC = 7200 - 1;
  TIM4->ARR = 10000 - 1;
  TIM4->EGR = TIM_EGR_UG;
  TIM4->SR = 0;
  TIM4->DIER = TIM_DIER_UIE;
  TIM4->CR1 = TIM_CR1_CEN;

  NVIC_SetPriority(TIM4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
  NVIC_EnableIRQ(TIM4_IRQn);

  for(;;) {

    #if 1

    #if USE_IWDG
      IWDG->KR = 0xAAAA;
    #endif

    if ((ADC1->SR & ADC_SR_EOC) == ADC_SR_EOC) {
      rbit[ndx] = ADC1->DR & 1;
      #if 0
      if (++ndx > 3) {
        ndx = 0;
        if ((rbit[0] != rbit[1]) && (rbit[2] != rbit[3]) && (rbit[0] != rbit[2])){
      #else
      if (++ndx > 1) {
        ndx = 0;
        if (rbit[0] != rbit[1]){
      #endif
          c = (c << 1) | (rbit[0] & 0x01);
          if (++rb_count > 256) {
            uint32_t a = __REV(c);
            // CRC->DR = (uint32_t) rand();
            // CRC->DR = c;
            // CRC->DR = (uint32_t) rand();
            //o_data[o_ndx] = CRC->DR /* c */;
            CRC->DR = ((a >> 16) | (a << 16)) ^ (uint32_t) rand();
            //o_data[o_ndx] = ((a >> 16) | (a << 16)) ^ (uint32_t) rand();
            __NOP();__NOP();__NOP();__NOP();__NOP();
            o_data[o_ndx] = CRC->DR;
            if (++o_ndx == O_SIZE) {
              o_ndx = 0;
              #if 0
              for(unsigned i = 0; i < O_SIZE; i++) {
                uint32_t x = o_data[i];
                for(unsigned j = 0; j < sizeof(uint32_t); j++) {
                  char ch = x & 0xFF;
                  out_buf[out_buf_ndx++] = TO_H(ch >> 4);  //(((VALUE) < 10) ? (VALUE) + '0' : (VALUE) + '7')
                  out_buf[out_buf_ndx++] = TO_H(ch & 0x0F);
                  x >>= 8;
                }
              }
              out_buf[out_buf_ndx++] = '\n';
              out_buf[out_buf_ndx] = 0;
              out_buf_ndx = 0;
              #else
              hex_string(out_buf, (char*)&o_data, O_SIZE * sizeof(uint32_t));
              #endif

              if (strlen(out_buf) > 0x85) {
                #if USE_UART
                  u_puts("--------------------------------------------------------------------------------------------");
                #endif
              } else {
                #if USE_UART
                  u_puts(out_buf);
                #endif
              }

              (void)ADC1->DR;

              //if (++line_count > (600000 - 1)) break;
              if (++line_count > (TOTAL_LINES - 1)) break;
            }
          } else {
            CRC->DR = c;
          }
        }




            #if 0
              CRC->DR = __REV(random_bits.random);
              CRC->DR = (unsigned) rand();
              random_bits.random = CRC->DR;
            #endif

            #if 0
            for (unsigned i = 0; i < sizeof(random_bits); i++) {
              u_putx(random_bits.rnd[i]);
            }
            u_putc(' ');
            if (++n_out == 8) {
              u_putc('\n');
              n_out = 0;
            }
            #endif
            //rb_count = 0;
            //(void)ADC1->DR;

          //}
        }
      }

    #else
      rbit[0] = rbit[1] = rbit[2] = rbit[3] = ndx = 0;
      CRC->DR = rb_count++;
      random_bits.random = CRC->DR;
      for (unsigned i = 0; i < sizeof(random_bits); i++) {
        u_putc(random_bits.rnd[i]);
      }
      #if USE_IWDG
        IWDG->KR = 0xAAAA;
      #endif
    #endif

    #if 0
    if (tripple_blink()) {

      // EXTI->SWIER = EXTI_SWIER_SWIER0;
      // EXTI->SWIER = EXTI_SWIER_SWIER1;

      time_struct_t t;
      uint32_t r = rtc_read_counter();
      // if (alarm_flag == '!') {
      //   rtc_set_alarm(r + 10);
      // }
      unixtime_to_time(r, &t);
      // uint8_t res = extract_randomness(&r);
      // if (res) {
      //   CRC->DR = r;
      // }
      // CRC->DR = (unsigned)rand();
      // r = CRC->DR;

      #if defined(__GNUC__) && !defined(__clang__)
        #define PRINT_FORMATTER "%02u.%02u.%u %02u:%02u:%02u #%08lX (%lu)\n"
      #else
        #define PRINT_FORMATTER "%02u.%02u.%u %02u:%02u:%02u #%08X\n"
      #endif

      // CRC->DR = random_bits;
      // CRC->DR = rb_count;

      uprintf(PRINT_FORMATTER, t.day, t.month, t.year, t.hour, t.minute, t.second, CRC->DR /*, rb_count */);
      //rb_count = 0;
      (void)ADC1->DR;
      //alarm_flag = ' ';
    }
    #endif

  }
  #if USE_UART
    u_puts("'''\n");
    u_puts(python_script_tail);
    uint32_t e_time = rtc_read_counter() /*- elapsed_time*/;

    #if defined(__GNUC__) && !defined(__clang__)
      uprintf("#Random bytes generated: %u, time elapsed: %lu seconds, throughput: %lu\n", TOTAL_LINES * BYTES_PER_LINE, e_time, TOTAL_LINES * BYTES_PER_LINE / e_time);
    #else
      uprintf("#Random bytes generated: %u, time elapsed: %u seconds, throughput: %u\n", TOTAL_LINES * BYTES_PER_LINE, e_time, TOTAL_LINES * BYTES_PER_LINE / e_time);
    #endif
  #endif
  for(;;){}

  /* Supress debug messages */

  #if defined (__CC_ARM) && !defined(__clang__)
    #pragma diag_suppress 111
  #endif

  #if defined (__ICCARM__)
    #pragma diag_suppress=Pe111
  #endif

  #if defined(__clang__)
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wunreachable-code-return"
  #endif

  return 0;

  #if defined(__clang__)
    #pragma clang diagnostic pop
  #endif

}

#define __EMPTY_LOOP()  do { __NOP(); } while(1)

#if defined(__clang__)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wmissing-prototypes"
  #pragma clang diagnostic ignored "-Wmissing-noreturn"
#endif

void NMI_Handler(void) {
  __EMPTY_LOOP();
}

void HardFault_Handler(void) {
  _SW(A, 0, HIGH);
  for(;;) {
    GPIOC->BSRR = GPIO_BSRR_BR13;
    DELAY_MS(50);
    GPIOC->BSRR = GPIO_BSRR_BS13;
    DELAY_MS(50);
  }
  // __EMPTY_LOOP();
}

void BusFault_Handler(void) {
  __EMPTY_LOOP();
}

void UsageFault_Handler(void) {
  __EMPTY_LOOP();
}

void SVC_Handler(void) {
  __EMPTY_LOOP();
}

void DebugMon_Handler(void) {
  __EMPTY_LOOP();
}

void PendSV_Handler(void) {
  //
}

void SysTick_Handler(void) {
  // TIM4->CR1 = 0;
  tick_period = TIM4->CNT;
  TIM4->EGR = TIM_EGR_UG;
  // TIM4->SR = 0;
  // TIM4->CR1 = TIM_CR1_CEN;
  s_cnt++;

  #if USE_BTN_IT
    static unsigned tick_divider;

    if (++tick_divider & 4) {
      if (is_key_pressed(READ_PIN(B, 12))) {
        key_pressed = !0;
      }
    }
  #endif

}

#if USE_TIM2
  void TIM2_IRQHandler(void) {
    if (TIM2->SR & TIM_SR_UIF) {
      TIM2->SR = ~TIM_SR_UIF;
      TIM2->CCR1 = (unsigned) rand() % TIM2->ARR;
      TIM2->CCR2 = (unsigned) rand() % TIM2->ARR;
      TIM2->CCR3 = (unsigned) rand() % TIM2->ARR;
      TIM2->CCR4 = (unsigned) rand() % TIM2->ARR;
    }
    #if TIM2_CH1_ENABLE
      if (TIM2->SR & TIM_SR_CC1IF) {
        TIM2->SR = ~TIM_SR_CC1IF;
      }
    #endif
    #if TIM2_CH2_ENABLE
      if (TIM2->SR & TIM_SR_CC2IF) {
        TIM2->SR = ~TIM_SR_CC2IF;
      }
    #endif
    #if TIM2_CH3_ENABLE
      if (TIM2->SR & TIM_SR_CC3IF) {
        TIM2->SR = ~TIM_SR_CC3IF;
      }
    #endif
    #if TIM2_CH4_ENABLE
      if (TIM2->SR & TIM_SR_CC4IF) {
        TIM2->SR = ~TIM_SR_CC4IF;

        #if TIM2_CH4_CAPTURE_MODE

          static unsigned char r[2], rn;
          static unsigned short ch4_prev;

          measured_period = (unsigned short) TIM2->CCR4 - ch4_prev;
          unsigned ndx = measured_period - 3410;

          // measured_period -= ch4_prev;
          // measured_period *= 3276806;
          // measured_period /= 1400;

          r[rn++] = TIM2->CCR4 & 1;
          if (rn > 1) {
            rn = 0;
            if (r[0] != r[1]) randomizer = (randomizer << 1) | r[0];
          }

          ch4_prev = (unsigned short) TIM2->CCR4;

          if (ndx < (sizeof(freq) / sizeof(unsigned))) {
            freq[ndx]++;
          }
        #endif
      }
    #endif
  }
#endif

#if USE_EXTI
  void EXTI0_IRQHandler(void) {               /* EXTI line0 interrupt handler routine                                                                                 */
    if (EXTI->PR & EXTI_PR_PR0) {
      EXTI->PR = EXTI_PR_PR0;
    }
  }

  void EXTI1_IRQHandler(void) {               /* EXTI line0 interrupt handler routine                                                                                 */
    if (EXTI->PR & EXTI_PR_PR1) {
      EXTI->PR = EXTI_PR_PR1;
    }
  }
#endif

#if defined(USE_RTC) && (USE_RTC != 0)
  #if (USE_RTC_SEC_IRQ != 0) || (USE_RTC_ALARM_IRQ != 0)
    void RTC_IRQHandler(void) {                   /* RTC global interrupt handler                                                                                       */

      #if USE_RTC_ALARM_IRQ != 0
        if (RTC->CRL & RTC_CRL_ALRF) {
          RTC->CRL &= ~RTC_CRL_ALRF;
          alarm_flag = '!';
          _SW(A, 1, HIGH);
        }
      #endif

      #if USE_RTC_SEC_IRQ != 0
        if (RTC->CRL & RTC_CRL_SECF) {
          t_cnt = s_cnt;
          RTC->CRL &= ~RTC_CRL_SECF;
          GPIOC->BSRR = GPIO_BSRR_BR13;
          s_cnt = 0;

          #if 0
            _SW(B, 5, HIGH);
          #endif

        }
      #endif
    }
  #endif

  #if USE_RTC_ALARM_EXTI_IRQ != 0
    void RTCAlarm_IRQHandler(void) {              /* RTC alarm interrupt handler                                                                                        */
      if (EXTI->PR & EXTI_PR_PR17) {
        EXTI->PR = EXTI_PR_PR17;
      }
    }
  #endif
#endif

extern volatile uint32_t t4_irq_cnt;
volatile uint32_t t4_irq_cnt = 0;
void TIM4_IRQHandler(void) {
  if ((TIM4->SR & TIM_SR_UIF) == TIM_SR_UIF) {
    TIM4->SR &= ~TIM_SR_UIF;
    t4_irq_cnt++;
  }
}

#if defined(__clang__)
  #pragma clang diagnostic pop
#endif
