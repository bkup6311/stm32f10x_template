#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f1xx.h"

#define USE_HSE                             1
#define USE_PLL                             1
#define USE_UART                            1
#define USE_TIM2                            1
#define USE_I2C                             1
#define USE_ADC                             1
#define USE_SPI                             1
#define USE_RTC                             1
#define USE_CRC                             1
#define USE_RNG                             1
//#define USE_DMA                             1
#define USE_IWDG                            1
#define USE_EXTI                            1
#define USE_BTN                             1
#define USE_SWD                             1


#if !defined(USE_RTC)
  #define USE_RTC 0
  #define USE_LSE 0
#else
  #if USE_RTC != 0
    #include "datetime.h"
    #define USE_LSE                         1
    #define USE_RTC_SEC_IRQ                 1
    #define USE_RTC_ALARM                   0
    #if USE_RTC_ALARM
      #define USE_RTC_ALARM_IRQ             1
      #define USE_RTC_ALARM_EXTI_IRQ        0
    #else
      #define USE_RTC_ALARM_IRQ             0
      #define USE_RTC_ALARM_EXTI_IRQ        0
    #endif
  #endif
#endif

#if !defined(USE_BTN)
  #define USE_BTN 0
  #define USE_BTN_IT 0
#else

  #define USE_BTN_IT 0

  __STATIC_INLINE unsigned is_key_pressed(unsigned pin_state) {

    enum {CURRENT = 0, PREVIOUS = 1, TOTAL = 2};
    static unsigned key_state[TOTAL];

    key_state[CURRENT] <<= 1;
    key_state[CURRENT] |= (pin_state != 0);

    unsigned s = ((key_state[CURRENT] == 0) && (key_state[PREVIOUS] != 0));
    key_state[PREVIOUS] = key_state[CURRENT];
    return s;
  }
#endif

#if !defined(USE_SWD)
  #define USE_SWD 1
#endif


#define PIN_CFG(PIN, MODE)                  ((MODE) << ((PIN) * 4))
                                                                              /*
    In input mode (MODE[1:0]=00):

  00: Analog mode
  01: Floating input (reset state)
  10: Input with pull-up / pull-down
  11: Reserved
                                                                              */
#define I_ANALOG                            (0ULL << 2)
#define I_FLOAT                             (1ULL << 2)
#define I_PULL                              (2ULL << 2)
                                                                              /*
    In output mode (MODE[1:0] > 00):

  00: General purpose output push-pull
  01: General purpose output Open-drain
  10: Alternate function output Push-pull
  11: Alternate function output Open-drain
                                                                              */
#define O_PP                                (0ULL << 2)
#define O_OD                                (1ULL << 2)
#define O_AF                                (2ULL << 2)
                                                                              /*
  MODEy[1:0]: Port x mode bits (y= 0 .. 7)
  These bits are written by software to configure the corresponding I/O port.
  Refer to Table 20: Port bit configuration table.

  00: Input mode (reset state)
  01: Output mode, max speed 10 MHz.
  10: Output mode, max speed 2 MHz.
  11: Output mode, max speed 50 MHz.
                                                                              */
#define O_10MHZ                             (1ULL)
#define O_2MHZ                              (2ULL)
#define O_50MHZ                             (3ULL)

#define PIN_HIGH(PIN_NO)                    GPIO_BSRR_BS ## PIN_NO
#define PIN_LOW(PIN_NO)                     GPIO_BSRR_BR ## PIN_NO

#define PULL_UP(PIN_NO)                     GPIO_BSRR_BS ## PIN_NO
#define PULL_DOWN(PIN_NO)                   GPIO_BSRR_BR ## PIN_NO

#define LOW                                 GPIO_BSRR_BR
#define HIGH                                GPIO_BSRR_BS

#define CAT(A, B)                           A ## B
#define CONCAT(A, B)                        CAT(A, B)
#define _SW(PORT, PIN, STATE)               GPIO ## PORT->BSRR = CAT(STATE, PIN)

#define READ_PIN(PORT, PIN)                 (GPIO ## PORT->IDR & GPIO_IDR_IDR ## PIN)

#if 0
#define ATOMIC_OUT(PORT, DATA)              PORT->BSRR = (unsigned)~(DATA) << 16 | (DATA)
#else
#define ATOMIC_OUT(PORT, DATA)              PORT->BSRR = 0x00FF0000 | (DATA)
#endif

#define HEX(NIBBLE)                         (((NIBBLE) < 10) ? (NIBBLE) + '0' : (NIBBLE) + '7')

#define KHZ                                 * 1000UL
#define MHZ                                 * 1000000UL

#define AHB_CLK                             SystemCoreClock

#if defined(USE_PLL) && USE_PLL
  #define APB1_CLK                          (AHB_CLK / 2)
#else
  #define APB1_CLK                          AHB_CLK
#endif

#define APB2_CLK                            AHB_CLK

#define GET_TICK()                          (SysTick->CTRL >> SysTick_CTRL_COUNTFLAG_Pos)
#define DELAY_MS(MS)                        do {(void)SysTick->CTRL; for(unsigned _ = MS; _; _ -= GET_TICK());}while(0)

#define BCD2DEC(BCD)                        ((BCD) - 6 * ((BCD) / 16))
#define DEC2BCD(DEC)                        ((DEC) + (((DEC) * 26) >> 8) * 6)


#if !defined(USE_I2C)
  #define USE_I2C 0
#else
  #if USE_I2C != 0
    #if 0
      #define I2C_SPEED_MODE                I2C_SPEED_STANDARD
    #else
      #define I2C_SPEED_MODE                I2C_SPEED_FAST
    #endif

      /* I2C bus speed  */
    #define I2C_SPEED_STANDARD              100 KHZ
    #define I2C_SPEED_FAST                  400 KHZ

      /* Fast Mode Duty Cycle */
    #define I2C_DUTY_CYCLE_2                0x00000000U             /* I2C fast mode Tlow/Thigh = 2        */
    #define I2C_DUTY_CYCLE_16_9             I2C_CCR_DUTY            /* I2C fast mode Tlow/Thigh = 16/9     */

    #define I2C_CONFIGURE_CCR(APB1_CLOCK, I2C_SPEED)                       (uint32_t)(((I2C_SPEED) <= I2C_SPEED_STANDARD)? \
                                                                           (I2C_CONFIGURE_CCR_STANDARD((APB1_CLOCK), (I2C_SPEED))) : \
                                                                           (I2C_CONFIGURE_CCR_FAST((APB1_CLOCK), (I2C_SPEED), (I2C_DUTY_CYCLE_2))))

    #define I2C_CONFIGURE_CCR_STANDARD(APB1_CLOCK, I2C_SPEED)              (uint32_t)(((((APB1_CLOCK)/((I2C_SPEED) << 1U)) & I2C_CCR_CCR) < 4U) ? 4U:((APB1_CLOCK) / ((I2C_SPEED) << 1U)))

    #define I2C_CONFIGURE_CCR_FAST(APB1_CLOCK, I2C_SPEED, I2C_DUTY_CYCLE)  (uint32_t)(((I2C_DUTY_CYCLE) == I2C_DUTY_CYCLE_2) ? \
                                                                           (((((APB1_CLOCK) / ((I2C_SPEED) * 3U)) & I2C_CCR_CCR) == 0U) ? 1U:((APB1_CLOCK) / ((I2C_SPEED) * 3U))) : \
                                                                           (((((APB1_CLOCK) / ((I2C_SPEED) * 25U)) & I2C_CCR_CCR) == 0U) ? 1U:((APB1_CLOCK) / ((I2C_SPEED) * 25U))))

    #define I2C_RISE_TIME(APB1_CLOCK, I2C_SPEED)                           (uint32_t)(((I2C_SPEED) <= I2C_SPEED_STANDARD) ? ((APB1_CLOCK) / 1000000 + 1) : ((((APB1_CLOCK) / 1000000) * 300) / 1000) + 1)
  #endif
#endif

#if !defined(USE_TIM2)
  #define USE_TIM2   0
#else
  #if 0
    #define TIM2_PSC               11200
  #else
    #define TIM2_PSC               0
  #endif

  #if 0
    #define TIM2_ARR               10000
  #else
    #define TIM2_ARR               0xFFFF
  #endif

  #define TIM2_CH1_ENABLE          0
  #define TIM2_CH2_ENABLE          0
  #define TIM2_CH3_ENABLE          0
  #define TIM2_CH4_ENABLE          1

  #if TIM2_CH1_ENABLE
    #define TIM2_CH1_CAPTURE_MODE  0
    #define TIM2_CH1_OUTPUT_ENABLE 0

    #if !TIM2_CH1_CAPTURE_MODE
      #define TIM2_CCR1            (((TIM2->ARR + 1) / 2) - 1)
    #else
      #define TIM2_CCR1            0
    #endif
  #else
    #define TIM2_CCR1              0
  #endif

  #if TIM2_CH2_ENABLE
    #define TIM2_CH2_CAPTURE_MODE  0
    #define TIM2_CH2_OUTPUT_ENABLE 0

    #if !TIM2_CH2_CAPTURE_MODE
      #define TIM2_CCR2            (((TIM2->ARR + 1) / 2) - 1)
    #else
      #define TIM2_CCR2            0
    #endif
  #else
    #define TIM2_CCR2              0
  #endif

  #if TIM2_CH3_ENABLE
    #define TIM2_CH3_CAPTURE_MODE  0
    #define TIM2_CH3_OUTPUT_ENABLE 0

    #if !TIM2_CH3_CAPTURE_MODE
      #define TIM2_CCR3            (((TIM2->ARR + 1) / 2) - 1)
    #else
      #define TIM2_CCR3            0
    #endif
  #else
    #define TIM2_CCR3              0
  #endif

  #if TIM2_CH4_ENABLE
    #define TIM2_CH4_CAPTURE_MODE  1
    #define TIM2_CH4_OUTPUT_ENABLE 0

    #if !TIM2_CH4_CAPTURE_MODE
      #define TIM2_CCR4            (((TIM2->ARR + 1) / 2) - 1)
    #else
      #define TIM2_CCR4            0
    #endif
  #else
    #define TIM2_CCR4              0
  #endif

#endif

#if !defined(USE_ADC)
  #define USE_ADC 0
#else
  #if USE_ADC != 0
    #define ADC_CHANNEL_0              0x00000000U
    #define ADC_CHANNEL_1              (                                                                        ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_2              (                                                      ADC_CR1_AWDCH_1                  )
    #define ADC_CHANNEL_3              (                                                      ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_4              (                                    ADC_CR1_AWDCH_2                                    )
    #define ADC_CHANNEL_5              (                                    ADC_CR1_AWDCH_2                   | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_6              (                                    ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1                  )
    #define ADC_CHANNEL_7              (                                    ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_8              (                  ADC_CR1_AWDCH_3                                                      )
    #define ADC_CHANNEL_9              (                  ADC_CR1_AWDCH_3                                     | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_10             (                  ADC_CR1_AWDCH_3                   | ADC_CR1_AWDCH_1                  )
    #define ADC_CHANNEL_11             (                  ADC_CR1_AWDCH_3                   | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_12             (                  ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2                                    )
    #define ADC_CHANNEL_13             (                  ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2                   | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_14             (                  ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1                  )
    #define ADC_CHANNEL_15             (                  ADC_CR1_AWDCH_3 | ADC_CR1_AWDCH_2 | ADC_CR1_AWDCH_1 | ADC_CR1_AWDCH_0)
    #define ADC_CHANNEL_16             (ADC_CR1_AWDCH_4                                                                        )
    #define ADC_CHANNEL_17             (ADC_CR1_AWDCH_4                                                       | ADC_CR1_AWDCH_0)

    #define ADC_SAMPLING_TIME_1_5      (0x00000000U                                           ) /* Sampling time   1.5 ADC clock cycle  */
    #define ADC_SAMPLING_TIME_7_5      (                                      ADC_SMPR2_SMP0_0) /* Sampling time   7.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_13_5     (                   ADC_SMPR2_SMP0_1                   ) /* Sampling time  13.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_28_5     (                   ADC_SMPR2_SMP0_1 | ADC_SMPR2_SMP0_0) /* Sampling time  28.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_41_5     (ADC_SMPR2_SMP0_2                                      ) /* Sampling time  41.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_55_5     (ADC_SMPR2_SMP0_2 |                    ADC_SMPR2_SMP0_0) /* Sampling time  55.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_71_5     (ADC_SMPR2_SMP0_2 | ADC_SMPR2_SMP0_1                   ) /* Sampling time  71.5 ADC clock cycles */
    #define ADC_SAMPLING_TIME_239_5    (ADC_SMPR2_SMP0_2 | ADC_SMPR2_SMP0_1 | ADC_SMPR2_SMP0_0) /* Sampling time 239.5 ADC clock cycles */

    #define ADC_SQR1(SEQ_NUM, CH_NUM)  ((ADC_CHANNEL_ ## CH_NUM << ADC_SQR1_SQ ## SEQ_NUM ##_Pos) & ADC_SQR1_SQ ## SEQ_NUM)
    #define ADC_SQR2(SEQ_NUM, CH_NUM)  ((ADC_CHANNEL_ ## CH_NUM << ADC_SQR2_SQ ## SEQ_NUM ##_Pos) & ADC_SQR2_SQ ## SEQ_NUM)
    #define ADC_SQR3(SEQ_NUM, CH_NUM)  ((ADC_CHANNEL_ ## CH_NUM << ADC_SQR3_SQ ## SEQ_NUM ##_Pos) & ADC_SQR3_SQ ## SEQ_NUM)
    #define ADC_SQR1_LEN(LEN)          (((uint32_t)((LEN) - 1) << ADC_SQR1_L_Pos) & ADC_SQR1_L)

    #define ADC_SMPR1(SEQ_NUM, CYCLES) ((ADC_SAMPLING_TIME_ ## CYCLES ## _5 << ADC_SMPR1_SMP ## SEQ_NUM ## _Pos) & ADC_SMPR1_SMP ## SEQ_NUM)
    #define ADC_SMPR2(SEQ_NUM, CYCLES) ((ADC_SAMPLING_TIME_ ## CYCLES ## _5 << ADC_SMPR2_SMP ## SEQ_NUM ## _Pos) & ADC_SMPR2_SMP ## SEQ_NUM)

    __STATIC_INLINE uint8_t extract_entropy(uint32_t * entropy) {

      uint8_t a, b, c, timeout = 100;
      uint32_t res = 0;
      (void) ADC1->DR;
      (void) SysTick->CTRL;

      for(unsigned i = 0; i < 32; i++) {
        do {
          do {
            if (!(timeout -= GET_TICK())) return 0;
            while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
              if (!(timeout -= GET_TICK())) return 0;
              /* Wait until conversion completes */
            }
            a = ADC1->DR & 1;
            while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
              if (!(timeout -= GET_TICK())) return 0;
              /* Wait until conversion completes */
            }
            b = ADC1->DR & 1;
          } while (a != b);

          do {
            if (!(timeout -= GET_TICK())) return 0;
            while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
              if (!(timeout -= GET_TICK())) return 0;
              /* Wait until conversion completes */
            }
            b = ADC1->DR & 1;
            while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
              if (!(timeout -= GET_TICK())) return 0;
              /* Wait until conversion completes */
            }
            c = ADC1->DR & 1;
          } while (b != c);
        } while (a != c);
        res = (res << 1) | a;
      }
      *entropy = res;
      return !0;
    }
  #endif
#endif

#if defined(USE_UART)
  #if USE_UART

    #define UART_PORT_NO                   3
    #define USE_UART_DMA                   0

    #if defined(USE_PLL) && USE_PLL
      #define USE_UART_BAUDRATE            921600UL
    #else
      #define USE_UART_BAUDRATE            115200UL
    #endif

    #define UART_PORT                      CONCAT(USART, UART_PORT_NO)
    #define UART_BAUDRATE(FCLK, BAUDRATE)  (((FCLK) + ((BAUDRATE)/2U)) / (BAUDRATE))

    #if UART_PORT_NO    == 1

      #define USE_UART1                    1
      #define USE_UART2                    0
      #define USE_UART3                    0

      #define UART_TX_DMA_CH               4
      #define UART_RX_DMA_CH               5

    #elif UART_PORT_NO  == 2

      #define USE_UART1                    0
      #define USE_UART2                    1
      #define USE_UART3                    0

      #define UART_TX_DMA_CH               7
      #define UART_RX_DMA_CH               6

    #elif UART_PORT_NO  == 3

      #define USE_UART1                    0
      #define USE_UART2                    0
      #define USE_UART3                    1

      #define UART_TX_DMA_CH               2
      #define UART_RX_DMA_CH               3

    #else
      #error USART Port selection error!
    #endif

    __STATIC_INLINE void u_putc(const char c) {
      while ((UART_PORT->SR & USART_SR_TXE) == 0) {
        /* JUST WAIT FOR TXE */
      }
      UART_PORT->DR = c;
    }

    #if USE_UART_DMA
      __STATIC_INLINE void init_dma_channel(DMA_Channel_TypeDef *ch, volatile unsigned par, volatile unsigned mar, unsigned count, unsigned ccr);
    #endif

    __STATIC_INLINE void u_puts(const char *s) {
      unsigned len = 0;
      while (s[len] != 0) {
        #if !defined(USE_UART_DMA) || (USE_UART_DMA == 0)
          u_putc(s[len]);
        #endif
        len++;
      }

      #if defined(USE_UART_DMA) && (USE_UART_DMA != 0)

        #define UART_TX_DMA CONCAT(DMA1_Channel, UART_TX_DMA_CH)
        #define UART_RX_DMA CONCAT(DMA1_Channel, UART_RX_DMA_CH)

        while (UART_TX_DMA->CNDTR) {
          /* Just wait till CNTDR becomes zerro */
        }

        init_dma_channel(UART_TX_DMA, (unsigned)&UART_PORT->DR, (unsigned)s, len, (
          1 * DMA_CCR_EN                    | /*  0x00000001 Channel enable                                                                                         */
          0 * DMA_CCR_TCIE                  | /*  0x00000002 Transfer complete interrupt enable                                                                     */
          0 * DMA_CCR_HTIE                  | /*  0x00000004 Half Transfer interrupt enable                                                                         */
          0 * DMA_CCR_TEIE                  | /*  0x00000008 Transfer error interrupt enable                                                                        */
          1 * DMA_CCR_DIR                   | /*  0x00000010 Data transfer direction                                                                                */
          0 * DMA_CCR_CIRC                  | /*  0x00000020 Circular mode                                                                                          */
          0 * DMA_CCR_PINC                  | /*  0x00000040 Peripheral increment mode                                                                              */
          1 * DMA_CCR_MINC                  | /*  0x00000080 Memory increment mode                                                                                  */
          0 * DMA_CCR_PSIZE                 | /*  0x00000300 PSIZE[1:0] bits (Peripheral size)                                                                      */
          0 * DMA_CCR_PSIZE_0               | /*    0x00000100                                                                                                      */
          0 * DMA_CCR_PSIZE_1               | /*    0x00000200                                                                                                      */
          0 * DMA_CCR_MSIZE                 | /*  0x00000C00 MSIZE[1:0] bits (Memory size)                                                                          */
          0 * DMA_CCR_MSIZE_0               | /*    0x00000400                                                                                                      */
          0 * DMA_CCR_MSIZE_1               | /*    0x00000800                                                                                                      */
          0 * DMA_CCR_PL                    | /*  0x00003000 PL[1:0] bits(Channel Priority level)                                                                   */
          0 * DMA_CCR_PL_0                  | /*    0x00001000                                                                                                      */
          0 * DMA_CCR_PL_1                  | /*    0x00002000                                                                                                      */
          0 * DMA_CCR_MEM2MEM                 /*  0x00004000 Memory to memory mode                                                                                  */
        ));

      #endif
    }

      /* Convert a nibble to HEX and send it via UART */
    __STATIC_INLINE void u_put_x(const uint8_t c) {
      /* Convert a nibble to HEX char and send it via UART */
      u_putc(HEX(c));
    }

      /* Convert a byte to HEX and send it via UART */
    __STATIC_INLINE void u_putx(const uint8_t c) {
      u_put_x(c >> 4);
      u_put_x(c & 0x0F);
    }

      /* MACRO to emulate printf() via UART */
    #define uprintf(...) do{char _[160]; snprintf(_, sizeof(_), __VA_ARGS__); u_puts(_);}while(0)
  #else
    #define USE_UART       0
    #define USE_USART1     0
    #define USE_USART2     0
    #define USE_USART3     0
    #define uprintf(...)
  #endif
#else
  #define USE_UART       0
  #define USE_USART1     0
  #define USE_USART2     0
  #define USE_USART3     0
  #define uprintf(...)
#endif

#if !defined(USE_CRC)
  #define USE_CRC 0
#endif

#if !defined(USE_IWDG)
  #define USE_IWDG 0
#endif

#if !defined(USE_SPI)
  #define USE_SPI 0
#else
  #if USE_SPI != 0
    __STATIC_INLINE char spi_xfer8(char d) {
      while((SPI1->SR & SPI_SR_TXE) != SPI_SR_TXE) {
        /* wait until spi transfer completes */
      }
      *((__IO char *)&SPI1->DR) = d;
      while((SPI1->SR & SPI_SR_RXNE) != SPI_SR_RXNE) {
        /* wait until spi transfer completes */
      }
      return *(__IO char *)&SPI1->DR;
    }
  #endif
#endif

#if !defined(USE_RNG)
  #define USE_RNG 0
#endif

#if !defined(USE_DMA) && USE_UART_DMA
    #define USE_DMA      !0
#endif

#if !defined(USE_DMA)
  #define USE_DMA 0
#else
  #if USE_DMA != 0
    __STATIC_INLINE void init_dma_channel(DMA_Channel_TypeDef *ch, volatile unsigned par, volatile unsigned mar, unsigned count, unsigned ccr) {
      ch->CCR = 0;
      ch->CNDTR = count;
      ch->CPAR = par;
      ch->CMAR = mar;
      ch->CCR = ccr;
    }
  #endif
#endif


__STATIC_INLINE void sysInit(void) {

  RCC->APB1ENR = (
    0 * RCC_APB1ENR_TIM3EN                  | /*  TIM3  clock enable                                                                                                */
    0 * RCC_APB1ENR_WWDGEN                  | /*  Window Watchdog clock enable                                                                                      */
    0 * RCC_APB1ENR_CAN1EN                  | /*  CAN1 clock enable                                                                                                 */

    USE_TIM2   * RCC_APB1ENR_TIM2EN         | /*  TIM2  clock enabled                                                                                               */
    USE_I2C    * RCC_APB1ENR_I2C1EN         | /*  I2C1  clock enable/disable                                                                                        */
    USE_RTC    * RCC_APB1ENR_BKPEN          | /*  Backup interface clock enable                                                                                     */
    USE_RTC    * RCC_APB1ENR_PWREN          | /*  Power interface clock enable                                                                                      */
    USE_RNG    * RCC_APB1ENR_TIM4EN         | /*  TIM4 clock enable                                                                                                 */
    USE_UART2  * RCC_APB1ENR_USART2EN       | /*  USART2  clock enable                                                                                              */
    USE_UART3  * RCC_APB1ENR_USART3EN       | /*  USART3  clock enable                                                                                              */

    0 * RCC_APB1ENR_SPI2EN                  | /*  SPI2  clock enable                                                                                                */
    0 * RCC_APB1ENR_I2C2EN                  | /*  I2C2  clock enable                                                                                                */
    0 * RCC_APB1ENR_USBEN                     /*  USB Device clock enable                                                                                           */
  );

  #if USE_RTC
    PWR->CR = PWR_CR_DBP;                     /*  0x00000100 Disable Backup Domain write protection                                                                 */

    RCC->BDCR = (
      USE_LSE * RCC_BDCR_LSEON              | /*  0x00000001 External Low Speed oscillator enable                                                                   */
      0 * RCC_BDCR_LSERDY                   | /*  0x00000002 External Low Speed oscillator Ready                                                                    */
      0 * RCC_BDCR_LSEBYP                   | /*  0x00000004 External Low Speed oscillator Bypass                                                                   */

      0 * RCC_BDCR_RTCSEL                   | /*  0x00000300 RTCSEL[1:0] bits (RTC clock source selection)                                                          */
      0 * RCC_BDCR_RTCSEL_0                 | /*    0x00000100                                                                                                      */
      0 * RCC_BDCR_RTCSEL_1                 | /*    0x00000200                                                                                                      */
      0 * RCC_BDCR_RTCSEL_NOCLOCK           | /*  No clock                                                                                                          */

      #if USE_LSE
        1 * RCC_BDCR_RTCSEL_LSE             | /*  LSE oscillator clock used as RTC clock                                                                            */
      #elif USE_HSE
        1 * RCC_BDCR_RTCSEL_HSE             | /*  HSE oscillator clock divided by 128 used as RTC clock                                                             */
      #else
        1 * RCC_BDCR_RTCSEL_LSI             | /*  LSI oscillator clock used as RTC clock                                                                            */
      #endif

      1 * RCC_BDCR_RTCEN                    | /*  0x00008000 RTC clock enable                                                                                       */
      0 * RCC_BDCR_BDRST                      /*  0x00010000 Backup domain software reset                                                                           */
    );
  #endif

  #if USE_RNG != 0
    TIM4->DIER = TIM_DIER_UIE;

    /* TIM4 interrupt Init */
    NVIC_SetPriority(TIM4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
    NVIC_EnableIRQ(TIM4_IRQn);

    TIM4->CR1 = TIM_CR1_CEN;
  #endif

  if ((RCC->CFGR & RCC_CFGR_SWS_PLL) == 0) {

    /*******************************************************************************************/
    /*               The system clock is configured as follows :                               */
    /*                                                                                         */
    /*                  System Clock source  = PLL (HSE)                                       */
    /*                  SYSCLK (Hz)          = 72000000                                        */
    /*                  HCLK (Hz)            = 72000000                                        */
    /*                  AHB Prescaler        = 1                                               */
    /*                  APB1 Prescaler       = 2                                               */
    /*                  APB2 Prescaler       = 1                                               */
    /*                  HSE Frequency (Hz)   = 8000000                                         */
    /*                  HSE PREDIV1          = 1                                               */
    /*                  PLLMUL               = 9                                               */
    /*                  Flash Latency (WS)   = 2                                               */
    /*******************************************************************************************/

    #if defined(USE_PLL) && USE_PLL
      #if defined(USE_HSE) && USE_HSE
        RCC->CFGR = (
          1 * RCC_CFGR_PLLSRC               | /*  HSE clock selected as PLL entry clock source                                                                      */
          1 * RCC_CFGR_PPRE1_DIV2           | /*  APB1 low-speed prescaler HCLK divided by 2 the software has to set correctly these bits to not exceed 36 MHz      */
          1 * RCC_CFGR_PLLMULL14              /*  PLL input clock * 9                                                                                               */
        );
      #else
        RCC->CFGR = (
          0 * RCC_CFGR_PLLSRC               | /*  HSI clock selected as PLL entry clock source                                                                      */
          1 * RCC_CFGR_PPRE1_DIV2           | /*  APB1 low-speed prescaler HCLK divided by 2 the software has to set correctly these bits to not exceed 36 MHz      */
          1 * RCC_CFGR_PLLMULL14              /*  PLL input clock * 9                                                                                               */
        );
      #endif
    #endif

    #if (defined(USE_HSE) && USE_HSE) || (defined(USE_PLL) && USE_PLL)
      RCC->CR = (
        0 * RCC_CR_HSION                    | /*  Internal High Speed clock enable                                                                                  */
        0 * RCC_CR_HSIRDY                   | /*  Internal High Speed clock ready flag                                                                              */

        #if 0
          0 * RCC_CR_HSITRIM                | /*  Internal High Speed clock trimming                                                                                */
        #else
          (0x10UL << RCC_CR_HSITRIM_Pos)    | /*  Default Internal high-speed clock trimming value                                                                  */
        #endif

        0 * RCC_CR_HSICAL                   | /*  Internal High Speed clock Calibration                                                                             */

        #if defined(USE_HSE) && USE_HSE
          RCC_CR_HSEON                      | /*  External High Speed clock enable                                                                                  */
        #endif

        0 * RCC_CR_HSERDY                   | /*  External High Speed clock ready flag                                                                              */
        0 * RCC_CR_HSEBYP                   | /*  External High Speed clock Bypass                                                                                  */
        0 * RCC_CR_CSSON                    | /*  Clock Security System enable                                                                                      */

        #if defined(USE_PLL) && USE_PLL
          RCC_CR_PLLON                      | /*  PLL enable                                                                                                        */
        #endif

        0 * RCC_CR_PLLRDY                     /*  PLL clock ready flag                                                                                              */
      );
    #endif

    #if defined(USE_HSE) && USE_HSE
      while(!(RCC->CR & RCC_CR_HSERDY)) {}    /*  Wait for External High Speed clock ready flag                                                                     */
    #endif

    #if defined(USE_PLL) && USE_PLL
      while(!(RCC->CR & RCC_CR_PLLRDY)) {}    /*  Wait for PLL clock ready flag                                                                                     */
      FLASH->ACR = (
        0 * FLASH_ACR_LATENCY               | /*  LATENCY[2:0] bits (Latency)                                                                                       */
        0 * FLASH_ACR_LATENCY_0             | /*    0x00000001                                                                                                      */
        0 * FLASH_ACR_LATENCY_1             | /*    0x00000002                                                                                                      */
        1 * FLASH_ACR_LATENCY_2             | /*    0x00000004                                                                                                      */

        0 * FLASH_ACR_HLFCYA                | /*  Flash Half Cycle Access Enable                                                                                    */
        1 * FLASH_ACR_PRFTBE                | /*  Prefetch Buffer Enable                                                                                            */
        0 * FLASH_ACR_PRFTBS                  /*  Prefetch Buffer Status                                                                                            */
      );
    #endif

    #if 0
      RCC->CFGR = (
        1 * RCC_CFGR_SW_PLL                 | /*  PLL selected as system clock                                                                                      */

        USE_ADC * RCC_CFGR_ADCPRE_DIV6      | / * ADC prescaler: PCLK2 divided by 6                                                                                 */

        1 * RCC_CFGR_PPRE1_DIV2               /*  APB1 low-speed prescaler HCLK divided by 2 the software has to set correctly these bits to not exceed 36 MHz      */
      );
    #else
      RCC->CFGR = (
        0 * RCC_CFGR_SW                     | /*  0x00000003 SW[1:0] bits (System clock Switch)                                                                     */
        0 * RCC_CFGR_SW_0                   | /*    0x00000001                                                                                                      */
        0 * RCC_CFGR_SW_1                   | /*    0x00000002                                                                                                      */

        #if defined(USE_PLL) && USE_PLL
          1 * RCC_CFGR_SW_PLL               | /*    PLL selected as system clock                                                                                    */
        #elif defined(USE_HSE) && USE_HSE
          1 * RCC_CFGR_SW_HSE               | /*    HSE selected as system clock                                                                                    */
        #else
          1 * RCC_CFGR_SW_HSI               | /*    HSI selected as system clock                                                                                    */
        #endif

        0 * RCC_CFGR_SWS                    | /*  0x0000000C SWS[1:0] bits (System Clock Switch Status)                                                             */
        0 * RCC_CFGR_SWS_0                  | /*    0x00000004                                                                                                      */
        0 * RCC_CFGR_SWS_1                  | /*    0x00000008                                                                                                      */
        0 * RCC_CFGR_SWS_HSI                | /*    HSI oscillator used as system clock                                                                             */
        0 * RCC_CFGR_SWS_HSE                | /*    HSE oscillator used as system clock                                                                             */
        0 * RCC_CFGR_SWS_PLL                | /*    PLL used as system clock                                                                                        */

        0 * RCC_CFGR_HPRE                   | /*  0x000000F0 HPRE[3:0] bits (AHB prescaler)                                                                         */
        0 * RCC_CFGR_HPRE_0                 | /*    0x00000010                                                                                                      */
        0 * RCC_CFGR_HPRE_1                 | /*    0x00000020                                                                                                      */
        0 * RCC_CFGR_HPRE_2                 | /*    0x00000040                                                                                                      */
        0 * RCC_CFGR_HPRE_3                 | /*    0x00000080                                                                                                      */
        0 * RCC_CFGR_HPRE_DIV1              | /*    SYSCLK not divided                                                                                              */
        0 * RCC_CFGR_HPRE_DIV2              | /*    SYSCLK divided by 2                                                                                             */
        0 * RCC_CFGR_HPRE_DIV4              | /*    SYSCLK divided by 4                                                                                             */
        0 * RCC_CFGR_HPRE_DIV8              | /*    SYSCLK divided by 8                                                                                             */
        0 * RCC_CFGR_HPRE_DIV16             | /*    SYSCLK divided by 16                                                                                            */
        0 * RCC_CFGR_HPRE_DIV64             | /*    SYSCLK divided by 64                                                                                            */
        0 * RCC_CFGR_HPRE_DIV128            | /*    SYSCLK divided by 128                                                                                           */
        0 * RCC_CFGR_HPRE_DIV256            | /*    SYSCLK divided by 256                                                                                           */
        0 * RCC_CFGR_HPRE_DIV512            | /*    SYSCLK divided by 512                                                                                           */

        0 * RCC_CFGR_PPRE1                  | /*  0x00000700 PRE1[2:0] bits (APB1 prescaler)                                                                        */
        0 * RCC_CFGR_PPRE1_0                | /*    0x00000100                                                                                                      */
        0 * RCC_CFGR_PPRE1_1                | /*    0x00000200                                                                                                      */
        0 * RCC_CFGR_PPRE1_2                | /*    0x00000400                                                                                                      */
        0 * RCC_CFGR_PPRE1_DIV1             | /*    HCLK not divided                                                                                                */

        #if defined(USE_PLL) && USE_PLL
          RCC_CFGR_PPRE1_DIV2               | /*    HCLK divided by 2                                                                                               */
        #endif

        0 * RCC_CFGR_PPRE1_DIV4             | /*    HCLK divided by 4                                                                                               */
        0 * RCC_CFGR_PPRE1_DIV8             | /*    HCLK divided by 8                                                                                               */
        0 * RCC_CFGR_PPRE1_DIV16            | /*    HCLK divided by 16                                                                                              */

        0 * RCC_CFGR_PPRE2                  | /*  0x00003800 PRE2[2:0] bits (APB2 prescaler)                                                                        */
        0 * RCC_CFGR_PPRE2_0                | /*    0x00000800                                                                                                      */
        0 * RCC_CFGR_PPRE2_1                | /*    0x00001000                                                                                                      */
        0 * RCC_CFGR_PPRE2_2                | /*    0x00002000                                                                                                      */
        0 * RCC_CFGR_PPRE2_DIV1             | /*    HCLK not divided                                                                                                */
        0 * RCC_CFGR_PPRE2_DIV2             | /*    HCLK divided by 2                                                                                               */
        0 * RCC_CFGR_PPRE2_DIV4             | /*    HCLK divided by 4                                                                                               */
        0 * RCC_CFGR_PPRE2_DIV8             | /*    HCLK divided by 8                                                                                               */
        0 * RCC_CFGR_PPRE2_DIV16            | /*    HCLK divided by 16                                                                                              */

        #if USE_ADC
          0 * RCC_CFGR_ADCPRE               | /*  0x0000C000 ADCPRE[1:0] bits (ADC prescaler)                                                                       */
          0 * RCC_CFGR_ADCPRE_0             | /*    0x00004000                                                                                                      */
          0 * RCC_CFGR_ADCPRE_1             | /*    0x00008000                                                                                                      */
          0 * RCC_CFGR_ADCPRE_DIV2          | /*    PCLK2 divided by 2                                                                                              */
          0 * RCC_CFGR_ADCPRE_DIV4          | /*    PCLK2 divided by 4                                                                                              */
          0 * RCC_CFGR_ADCPRE_DIV6          | /*    PCLK2 divided by 6                                                                                              */
          1 * RCC_CFGR_ADCPRE_DIV8          | /*    PCLK2 divided by 8                                                                                              */
        #endif /* USE_ADC */

        0 * RCC_CFGR_PLLSRC                 | /*  0x00010000 PLL entry clock source.                          This bit can be written only when PLL is disabled.    */
        0 * RCC_CFGR_PLLXTPRE               | /*  0x00020000 HSE divider for PLL entry.                       This bit can be written only when PLL is disabled.    */
        0 * RCC_CFGR_PLLXTPRE_HSE           | /*    HSE clock not divided for PLL entry                                                                             */
        0 * RCC_CFGR_PLLXTPRE_HSE_DIV2      | /*    HSE clock divided by 2 for PLL entry                                                                            */

        0 * RCC_CFGR_PLLMULL                | /*  0x003C0000 PLLMUL[3:0] bits (PLL multiplication factor)     These bits can be written only when PLL is disabled.  */
        0 * RCC_CFGR_PLLMULL_0              | /*    0x00040000                                                                                                      */
        0 * RCC_CFGR_PLLMULL_1              | /*    0x00080000                                                                                                      */
        0 * RCC_CFGR_PLLMULL_2              | /*    0x00100000                                                                                                      */
        0 * RCC_CFGR_PLLMULL_3              | /*    0x00200000                                                                                                      */
        0 * RCC_CFGR_PLLMULL2               | /*    0x00000000 PLL input clock * 2                                                                                  */
        0 * RCC_CFGR_PLLMULL3               | /*    0x00040000 PLL input clock * 3                                                                                  */
        0 * RCC_CFGR_PLLMULL4               | /*    0x00080000 PLL input clock * 4                                                                                  */
        0 * RCC_CFGR_PLLMULL5               | /*    0x000C0000 PLL input clock * 5                                                                                  */
        0 * RCC_CFGR_PLLMULL6               | /*    0x00100000 PLL input clock * 6                                                                                  */
        0 * RCC_CFGR_PLLMULL7               | /*    0x00140000 PLL input clock * 7                                                                                  */
        0 * RCC_CFGR_PLLMULL8               | /*    0x00180000 PLL input clock * 8                                                                                  */
        0 * RCC_CFGR_PLLMULL9               | /*    0x001C0000 PLL input clock * 9                                                                                  */
        0 * RCC_CFGR_PLLMULL10              | /*    0x00200000 PLL input clock * 10                                                                                 */
        0 * RCC_CFGR_PLLMULL11              | /*    0x00240000 PLL input clock * 11                                                                                 */
        0 * RCC_CFGR_PLLMULL12              | /*    0x00280000 PLL input clock * 12                                                                                 */
        0 * RCC_CFGR_PLLMULL13              | /*    0x002C0000 PLL input clock * 13                                                                                 */
        0 * RCC_CFGR_PLLMULL14              | /*    0x00300000 PLL input clock * 14                                                                                 */
        0 * RCC_CFGR_PLLMULL15              | /*    0x00340000 PLL input clock * 15                                                                                 */
        0 * RCC_CFGR_PLLMULL16              | /*    0x00380000 PLL input clock * 16                                                                                 */

        0 * RCC_CFGR_USBPRE                 | /*  0x00400000 USB Device prescaler                                                                                   */

        0 * RCC_CFGR_MCO                    | /*  0x07000000 MCO[2:0] bits (Microcontroller Clock Output)                                                           */
        0 * RCC_CFGR_MCO_0                  | /*    0x01000000                                                                                                      */
        0 * RCC_CFGR_MCO_1                  | /*    0x02000000                                                                                                      */
        0 * RCC_CFGR_MCO_2                  | /*    0x04000000                                                                                                      */
        0 * RCC_CFGR_MCO_NOCLOCK            | /*    No clock                                                                                                        */
        0 * RCC_CFGR_MCO_SYSCLK             | /*    System clock selected as MCO source                                                                             */
        0 * RCC_CFGR_MCO_HSI                | /*    HSI clock selected as MCO source                                                                                */
        0 * RCC_CFGR_MCO_HSE                | /*    HSE clock selected as MCO source                                                                                */
        0 * RCC_CFGR_MCO_PLLCLK_DIV2          /*    PLL clock divided by 2 selected as MCO source                                                                   */
      );
    #endif

    #if defined(USE_PLL) && USE_PLL
      while(!(RCC->CFGR & RCC_CFGR_SWS_PLL)){}/*  wait for PLL is used as system clock                                                                              */
    #elif defined(USE_HSE) && USE_HSE
      while(!(RCC->CFGR & RCC_CFGR_SWS_HSE)){}/*  wait for PLL is used as system clock                                                                              */
    #endif
  }        /*        === End of the Clock configuration routine ===                 */

  RCC->AHBENR = (
    1 * RCC_AHBENR_SRAMEN                   | /*  SRAM interface clock enable                                                                                       */
    1 * RCC_AHBENR_FLITFEN                  | /*  FLITF clock enable                                                                                                */

    USE_DMA * RCC_AHBENR_DMA1EN             | /*  DMA1 clock enable                                                                                                 */
    USE_CRC * RCC_AHBENR_CRCEN                /*  CRC clock enable                                                                                                  */
  );

  RCC->APB2ENR = (
    1 * RCC_APB2ENR_AFIOEN                  | /*  Alternate Function I/O clock enable                                                                               */
    1 * RCC_APB2ENR_IOPAEN                  | /*  I/O port A clock enable                                                                                           */
    1 * RCC_APB2ENR_IOPBEN                  | /*  I/O port B clock enable                                                                                           */
    1 * RCC_APB2ENR_IOPCEN                  | /*  I/O port C clock enable                                                                                           */
    0 * RCC_APB2ENR_IOPDEN                  | /*  I/O port D clock enable                                                                                           */

    USE_ADC   * RCC_APB2ENR_ADC1EN          | /*  ADC1  interface clock enable                                                                                      */
    USE_SPI   * RCC_APB2ENR_SPI1EN          | /*  SPI1  clock enable                                                                                                */
    USE_UART1 * RCC_APB2ENR_USART1EN        | /*  USART1 clock enable                                                                                               */

    0 * RCC_APB2ENR_ADC2EN                  | /*  ADC2  interface clock enable                                                                                      */
    0 * RCC_APB2ENR_TIM1EN                  | /*  TIM1 Timer clock enable                                                                                           */
    0 * RCC_APB2ENR_IOPEEN                    /*  I/O port E clock enable                                                                                           */
  );

  GPIOA->BSRR = (                             /*  Set initial state, pull-up and pull-down mode for port A pins                                                     */
    #if USE_UART
      #if USE_UART1 == 1
        PULL_DOWN(10)                       | /*  Configure USART1 RX pin in Pull Up mode                                                                           */
      #elif USE_UART2 == 1
        PULL_DOWN(3)                        | /*  Configure USART2 RX pin in Pull Down mode                                                                         */
      #endif
    #endif

    #if TIM2_CH4_ENABLE
      #if TIM2_CH4_CAPTURE_MODE
        PULL_UP(3)                          | /*  Configure PA3 in Pull-Up mode                                                                                     */
      #endif
    #endif

    #if USE_SWD
      PULL_UP(13)                           | /*  Configure SWDIO pin in Pull Up mode                                                                               */
      PULL_DOWN(14)                         | /*  Configure SWDCLK pin in Pull Down mode                                                                            */
    #endif

    0
  );

  #if defined(__clang__)
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wcast-align"
  #endif

    #define TIM2_CH4_CAPTURE_MODE  1
    #define TIM2_CH4_OUTPUT_ENABLE 0


  *(volatile uint64_t *) GPIOA = (
    #if defined(USE_TIM2) && (USE_TIM2 != 0)
      #if TIM2_CH1_ENABLE
        #if !TIM2_CH1_CAPTURE_MODE
          #if TIM2_CH1_OUTPUT_ENABLE
            PIN_CFG(0,  O_AF + O_10MHZ)     | /*  PA0:  TIM2 CHANNEL 1 OUTPUT (PUSH-PULL, MEDIUM SPEEED)                                                            */
          #else
            PIN_CFG(0,  O_2MHZ)             | /*  PA0:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
          #endif
        #else
          PIN_CFG(0,  I_FLOAT)              | /*  PA0:  INPUT, FLOAT                                                                                                */
        #endif
      #else
        PIN_CFG(0,  O_2MHZ)                 | /*  PA0:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      #endif

      #if TIM2_CH2_ENABLE
        #if !TIM2_CH1_CAPTURE_MODE
          #if TIM2_CH1_OUTPUT_ENABLE
            PIN_CFG(1,  O_AF + O_10MHZ)     | /*  PA1:  TIM2 CHANNEL 2 OUTPUT (PUSH-PULL, MEDIUM SPEEED)                                                            */
          #else
            PIN_CFG(1,  O_2MHZ)             | /*  PA1:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
          #endif
        #else
          PIN_CFG(1,  I_FLOAT)              | /*  PA1:  INPUT, FLOAT                                                                                                */
        #endif
      #else
        PIN_CFG(1,  O_2MHZ)                 | /*  PA1:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      #endif
    #else
      PIN_CFG(0,  O_2MHZ)                   | /*  PA0:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      PIN_CFG(1,  O_2MHZ)                   | /*  PA1:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
    #endif

    #if defined(USE_UART) && USE_UART2
      PIN_CFG(2,  O_AF + O_10MHZ)           | /*  PA2:  USART2 TX (PUSH-PULL, MEDIUM SPEEED)                                                                        */
      PIN_CFG(3,  I_PULL)                   | /*  PA3:  USART2 RX (INPUT, PULL-DOWN)                                                                                */
    #elif defined(USE_TIM2) && USE_TIM2
      #if TIM2_CH3_ENABLE
        #if !TIM2_CH3_CAPTURE_MODE
          #if TIM2_CH3_OUTPUT_ENABLE
            PIN_CFG(2,  O_AF + O_10MHZ)     | /*  PA2:  TIM2 CHANNEL 2 OUTPUT (PUSH-PULL, MEDIUM SPEEED)                                                            */
          #else
            PIN_CFG(2,  O_2MHZ)             | /*  PA2:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
          #endif
        #else
          PIN_CFG(2,  I_FLOAT)              | /*  PA2:  INPUT, FLOAT                                                                                                */
        #endif
      #else
        PIN_CFG(2,  O_2MHZ)                 | /*  PA2:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      #endif

      #if TIM2_CH4_ENABLE
        #if !TIM2_CH4_CAPTURE_MODE
          #if TIM2_CH4_OUTPUT_ENABLE
            PIN_CFG(3,  O_AF + O_10MHZ)     | /*  PA3:  TIM2 CHANNEL 2 OUTPUT (PUSH-PULL, MEDIUM SPEEED)                                                            */
          #else
            PIN_CFG(3,  O_2MHZ)             | /*  PA3:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
          #endif
        #else
          PIN_CFG(3,  I_PULL)               | /*  PA3:  INPUT, PULL-UP                                                                                              */
        #endif
      #else
        PIN_CFG(3,  O_2MHZ)                 | /*  PA3:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      #endif
    #else
      PIN_CFG(2,  O_2MHZ)                   | /*  PA2:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      PIN_CFG(3,  O_2MHZ)                   | /*  PA3:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
    #endif

    #if USE_SPI
      PIN_CFG(4,  O_10MHZ)                  | /*  PA4:  OUTPUT (PUSH-PULL, LOW SPEEED), SPI1 CS                                                                     */
      PIN_CFG(5,  O_AF + O_50MHZ)           | /*  PA5:  SPI1_SCK  (PUSH-PULL, HIGH SPEEED)                                                                          */
      PIN_CFG(6,  I_FLOAT)                  | /*  PA6:  SPI1_MISO (INPUT, NO PULL)                                                                                  */
      PIN_CFG(7,  O_AF + O_50MHZ)           | /*  PA7:  SPI1_MOSI (PUSH-PULL, HIGH SPEEED)                                                                          */
    #else
      PIN_CFG(4,  O_2MHZ)                   | /*  PA4:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      PIN_CFG(5,  O_2MHZ)                   | /*  PA5:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      PIN_CFG(6,  O_2MHZ)                   | /*  PA6:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
      PIN_CFG(7,  O_2MHZ)                   | /*  PA7:  OUTPUT (PUSH-PULL, LOW SPEEED)                                                                              */
    #endif

    PIN_CFG(8,  I_ANALOG)                   | /*                                      - unused -                                                                    */

    #if defined(USE_UART) && (USE_UART1 == 1)
      PIN_CFG(9,  O_AF + O_10MHZ)           | /*  PA9:  USART1 TX (PUSH-PULL, MEDIUM SPEEED)                                                                        */
      PIN_CFG(10, I_PULL)                   | /*  PA10: USART1 RX (INPUT, PULL-DOWN)                                                                                */
    #else
      PIN_CFG(9,  I_ANALOG)                 | /*                                      - unused -                                                                    */
      PIN_CFG(10, I_ANALOG)                 | /*                                      - unused -                                                                    */
    #endif

    PIN_CFG(11, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(12, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(13, I_PULL)                     | /*  PA13: SWDIO (INPUT, PULL-UP)                                                                                      */
    PIN_CFG(14, I_PULL)                     | /*  PA14: SWCLK (INPUT, PULL-DOWN)                                                                                    */
    PIN_CFG(15, I_ANALOG)                     /*                                      - unused -                                                                    */
  );

  GPIOB->BSRR = (                             /*  Set initial state, pull-up and pull-down mode for port B pins                                                     */
    #if USE_EXTI
      PULL_UP(0)                            | /*  Set pull-up mode for B0 EXTI Line                                                                                 */
      PULL_UP(1)                            | /*  Set pull-up mode for B1 EXTI Line                                                                                 */
    #endif

    #if USE_I2C
      PIN_HIGH(6)                           | /*  Set SCL Line HIGH                                                                                                 */
      PIN_HIGH(7)                           | /*  SET SDA Line HIGH                                                                                                 */
    #endif

    #if defined(USE_UART) && (USE_UART3 == 1)
      PULL_DOWN(11)                         | /*  Configure USART3 RX pin in Pull-Down mode                                                                         */
    #endif

    #if USE_BTN
      PULL_UP(12)                           | /*  Configure PB12 pin in Pull-Up mode                                                                                */
    #endif
    0
  );

  *(volatile uint64_t *) GPIOB = (

    #if USE_EXTI
      PIN_CFG(0,  I_PULL)                   | /*  PB0: EXTI Line 0                                                                                                  */
      PIN_CFG(1,  I_PULL)                   | /*  PB1: EXTI Line 1                                                                                                  */
    #else
      PIN_CFG(0,  I_ANALOG)                 | /*                                      - unused -                                                                    */
      PIN_CFG(1,  I_ANALOG)                 | /*                                      - unused -                                                                    */
    #endif

    PIN_CFG(2,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(3,  I_ANALOG)                   | /*                                      - unused -                                                                    */

    #if 0
      PIN_CFG(4,  I_ANALOG)                 | /*                                      - unused -                                                                    */
      PIN_CFG(5,  I_ANALOG)                 | /*                                      - unused -                                                                    */
    #else
      PIN_CFG(4,  O_10MHZ)                  | /*  PB4: OUTPUT, PUSH-PULL, MEDIUM SPEEED                                                                             */
      PIN_CFG(5,  O_10MHZ)                  | /*  PB5: OUTPUT, PUSH-PULL, MEDIUM SPEEED                                                                             */
    #endif

    #if USE_I2C
      PIN_CFG(6,  O_AF + O_OD + O_10MHZ)    | /*  PB6:  I2C1 SCL (OPEN DRAIN, MEDIUM SPEEED)                                                                        */
      PIN_CFG(7,  O_AF + O_OD + O_10MHZ)    | /*  PB7:  I2C1 SDA (OPEN DRAIN, MEDIUM SPEEED)                                                                        */
    #else
      PIN_CFG(6,  I_ANALOG)                 | /*                                      - unused -                                                                    */
      PIN_CFG(7,  I_ANALOG)                 | /*                                      - unused -                                                                    */
    #endif

    PIN_CFG(8,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(9,  I_ANALOG)                   | /*                                      - unused -                                                                    */

    #if defined(USE_UART) && (USE_UART3 == 1)
      PIN_CFG(10,  O_AF + O_10MHZ)          | /*  PB10: USART3 TX (PUSH-PULL, MEDIUM SPEEED)                                                                        */
      PIN_CFG(11, I_PULL)                   | /*  PA11: USART3 RX (INPUT, PULL-DOWN)                                                                                */
    #else
      PIN_CFG(10, I_ANALOG)                 | /*                                      - unused -                                                                    */
      PIN_CFG(11, I_ANALOG)                 | /*                                      - unused -                                                                    */
    #endif

    #if USE_BTN
      PIN_CFG(12, I_PULL)                   | /*                                      - unused -                                                                    */
    #else
      PIN_CFG(12, I_ANALOG)                 | /*                                      - unused -                                                                    */
    #endif

    PIN_CFG(13, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(14, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(15, I_ANALOG)                     /*                                      - unused -                                                                    */
  );

  GPIOC->BSRR = PIN_HIGH(13);                 /*  SET BLINK PIN HIGH (LED OFF)                                                                                      */

  *(volatile uint64_t *) GPIOC = (
    PIN_CFG(0,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(1,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(2,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(3,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(4,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(5,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(6,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(7,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(8,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(9,  I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(10, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(11, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(12, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(13, O_2MHZ)                     | /*  PC13: OUTPUT (PUSH-PULL, LOW SPEED)                                                                               */
    PIN_CFG(14, I_ANALOG)                   | /*                                      - unused -                                                                    */
    PIN_CFG(15, I_ANALOG)                     /*                                      - unused -                                                                    */
  );

  #if defined(__clang__)
    #pragma clang diagnostic pop
  #endif

  AFIO->MAPR = (
    0 * AFIO_MAPR_SPI1_REMAP                | /*  0x00000001 SPI1 remapping                                                                                         */
    0 * AFIO_MAPR_I2C1_REMAP                | /*  0x00000002 I2C1 remapping                                                                                         */
    0 * AFIO_MAPR_USART1_REMAP              | /*  0x00000004 USART1 remapping                                                                                       */
    0 * AFIO_MAPR_USART2_REMAP              | /*  0x00000008 USART2 remapping                                                                                       */

    0 * AFIO_MAPR_USART3_REMAP              | /*  0x00000030 USART3_REMAP[1:0] bits (USART3 remapping)                                                              */
    0 * AFIO_MAPR_USART3_REMAP_0            | /*    0x00000010                                                                                                      */
    0 * AFIO_MAPR_USART3_REMAP_1            | /*    0x00000020                                                                                                      */

    0 * AFIO_MAPR_USART3_REMAP_NOREMAP      | /*  No remap (TX/PB10, RX/PB11, CK/PB12, CTS/PB13, RTS/PB14)                                                          */
    0 * AFIO_MAPR_USART3_REMAP_PARTIALREMAP | /*  0x00000010 Partial remap (TX/PC10, RX/PC11, CK/PC12, CTS/PB13, RTS/PB14)                                          */
    0 * AFIO_MAPR_USART3_REMAP_FULLREMAP    | /*  0x00000030 Full remap (TX/PD8, RX/PD9, CK/PD10, CTS/PD11, RTS/PD12)                                               */

    0 * AFIO_MAPR_TIM1_REMAP                | /*  0x000000C0 TIM1_REMAP[1:0] bits (TIM1 remapping)                                                                  */
    0 * AFIO_MAPR_TIM1_REMAP_0              | /*    0x00000040                                                                                                      */
    0 * AFIO_MAPR_TIM1_REMAP_1              | /*    0x00000080                                                                                                      */

    0 * AFIO_MAPR_TIM1_REMAP_NOREMAP        | /*  No remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PB12, CH1N/PB13, CH2N/PB14, CH3N/PB15)             */
    0 * AFIO_MAPR_TIM1_REMAP_PARTIALREMAP   | /*  0x00000040 Partial remap (ETR/PA12, CH1/PA8, CH2/PA9, CH3/PA10, CH4/PA11, BKIN/PA6, CH1N/PA7, CH2N/PB0, CH3N/PB1) */
    0 * AFIO_MAPR_TIM1_REMAP_FULLREMAP      | /*  0x000000C0 Full remap (ETR/PE7, CH1/PE9, CH2/PE11, CH3/PE13, CH4/PE14, BKIN/PE15, CH1N/PE8, CH2N/PE10, CH3N/PE12) */

    0 * AFIO_MAPR_TIM2_REMAP                | /*  0x00000300 TIM2_REMAP[1:0] bits (TIM2 remapping)                                                                  */
    0 * AFIO_MAPR_TIM2_REMAP_0              | /*    0x00000100                                                                                                      */
    0 * AFIO_MAPR_TIM2_REMAP_1              | /*    0x00000200                                                                                                      */

    0 * AFIO_MAPR_TIM2_REMAP_NOREMAP        | /*  No remap (CH1/ETR/PA0, CH2/PA1, CH3/PA2, CH4/PA3)                                                                 */
    0 * AFIO_MAPR_TIM2_REMAP_PARTIALREMAP1  | /*  0x00000100 Partial remap (CH1/ETR/PA15, CH2/PB3, CH3/PA2, CH4/PA3)                                                */
    0 * AFIO_MAPR_TIM2_REMAP_PARTIALREMAP2  | /*  0x00000200 Partial remap (CH1/ETR/PA0, CH2/PA1, CH3/PB10, CH4/PB11)                                               */
    0 * AFIO_MAPR_TIM2_REMAP_FULLREMAP      | /*  0x00000300 Full remap (CH1/ETR/PA15, CH2/PB3, CH3/PB10, CH4/PB11)                                                 */

    0 * AFIO_MAPR_TIM3_REMAP                | /*  0x00000C00 TIM3_REMAP[1:0] bits (TIM3 remapping)                                                                  */
    0 * AFIO_MAPR_TIM3_REMAP_0              | /*    0x00000400                                                                                                      */
    0 * AFIO_MAPR_TIM3_REMAP_1              | /*    0x00000800                                                                                                      */

    0 * AFIO_MAPR_TIM3_REMAP_NOREMAP        | /*  No remap (CH1/PA6, CH2/PA7, CH3/PB0, CH4/PB1)                                                                     */
    0 * AFIO_MAPR_TIM3_REMAP_PARTIALREMAP   | /*  0x00000800 Partial remap (CH1/PB4, CH2/PB5, CH3/PB0, CH4/PB1)                                                     */
    0 * AFIO_MAPR_TIM3_REMAP_FULLREMAP      | /*  0x00000C00 Full remap (CH1/PC6, CH2/PC7, CH3/PC8, CH4/PC9)                                                        */

    0 * AFIO_MAPR_TIM4_REMAP                | /*  0x00001000 TIM4_REMAP bit (TIM4 remapping)                                                                        */

    0 * AFIO_MAPR_CAN_REMAP                 | /*  0x00006000 CAN_REMAP[1:0] bits (CAN Alternate function remapping)                                                 */
    0 * AFIO_MAPR_CAN_REMAP_0               | /*    0x00002000                                                                                                      */
    0 * AFIO_MAPR_CAN_REMAP_1               | /*    0x00004000                                                                                                      */

    0 * AFIO_MAPR_CAN_REMAP_REMAP1          | /*  CANRX mapped to PA11, CANTX mapped to PA12                                                                        */
    0 * AFIO_MAPR_CAN_REMAP_REMAP2          | /*  0x00004000 CANRX mapped to PB8, CANTX mapped to PB9                                                               */
    0 * AFIO_MAPR_CAN_REMAP_REMAP3          | /*  0x00006000 CANRX mapped to PD0, CANTX mapped to PD1                                                               */

    0 * AFIO_MAPR_PD01_REMAP                | /*  0x00008000 Port D0/Port D1 mapping on OSC_IN/OSC_OUT                                                              */

    0 * AFIO_MAPR_SWJ_CFG                   | /*  0x07000000 SWJ_CFG[2:0] bits (Serial Wire JTAG configuration)                                                     */
    0 * AFIO_MAPR_SWJ_CFG_0                 | /*    0x01000000                                                                                                      */
    0 * AFIO_MAPR_SWJ_CFG_1                 | /*    0x02000000                                                                                                      */
    0 * AFIO_MAPR_SWJ_CFG_2                 | /*    0x04000000                                                                                                      */

    0 * AFIO_MAPR_SWJ_CFG_RESET             | /*  Full SWJ (JTAG-DP + SW-DP) : Reset State                                                                          */
    0 * AFIO_MAPR_SWJ_CFG_NOJNTRST          | /*  0x01000000 Full SWJ (JTAG-DP + SW-DP) but without JNTRST                                                          */
    1 * AFIO_MAPR_SWJ_CFG_JTAGDISABLE       | /*  0x02000000 JTAG-DP Disabled and SW-DP Enabled                                                                     */
    0 * AFIO_MAPR_SWJ_CFG_DISABLE             /*  0x04000000 JTAG-DP Disabled and SW-DP Disabled                                                                    */
  );

  #if USE_EXTI
    #define INIT_AFIO_EXTICR_0   (          \
      0 * AFIO_EXTICR1_EXTI0                | /*  0x0000000F EXTI 0 configuration                                                                                   */\
      0 * AFIO_EXTICR1_EXTI1                | /*  0x000000F0 EXTI 1 configuration                                                                                   */\
      0 * AFIO_EXTICR1_EXTI2                | /*  0x00000F00 EXTI 2 configuration                                                                                   */\
      0 * AFIO_EXTICR1_EXTI3                | /*  0x0000F000 EXTI 3 configuration                                                                                   */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR1_EXTI0_PA             | /*  0x00000000 PA[0] pin                                                                                              */\
      1 * AFIO_EXTICR1_EXTI0_PB             | /*  0x00000001 PB[0] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI0_PC             | /*  0x00000002 PC[0] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI0_PD             | /*  0x00000003 PD[0] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI0_PE             | /*  0x00000004 PE[0] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI0_PF             | /*  0x00000005 PF[0] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI0_PG             | /*  0x00000006 PG[0] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR1_EXTI1_PA             | /*  0x00000000 PA[1] pin                                                                                              */\
      1 * AFIO_EXTICR1_EXTI1_PB             | /*  0x00000010 PB[1] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI1_PC             | /*  0x00000020 PC[1] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI1_PD             | /*  0x00000030 PD[1] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI1_PE             | /*  0x00000040 PE[1] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI1_PF             | /*  0x00000050 PF[1] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI1_PG             | /*  0x00000060 PG[1] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR1_EXTI2_PA             | /*  0x00000000 PA[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PB             | /*  0x00000100 PB[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PC             | /*  0x00000200 PC[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PD             | /*  0x00000300 PD[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PE             | /*  0x00000400 PE[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PF             | /*  0x00000500 PF[2] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI2_PG             | /*  0x00000600 PG[2] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR1_EXTI3_PA             | /*  0x00000000 PA[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PB             | /*  0x00001000 PB[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PC             | /*  0x00002000 PC[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PD             | /*  0x00003000 PD[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PE             | /*  0x00004000 PE[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PF             | /*  0x00005000 PF[3] pin                                                                                              */\
      0 * AFIO_EXTICR1_EXTI3_PG               /*  0x00006000 PG[3] pin                                                                                              */\
    )
    #if INIT_AFIO_EXTICR_0
      AFIO->EXTICR[0] = INIT_AFIO_EXTICR_0;
    #endif
    #undef INIT_AFIO_EXTICR_0

    #define INIT_AFIO_EXTICR_1   (          \
      0 * AFIO_EXTICR2_EXTI4                | /*  0x0000000F EXTI 4 configuration                                                                                   */\
      0 * AFIO_EXTICR2_EXTI5                | /*  0x000000F0 EXTI 5 configuration                                                                                   */\
      0 * AFIO_EXTICR2_EXTI6                | /*  0x00000F00 EXTI 6 configuration                                                                                   */\
      0 * AFIO_EXTICR2_EXTI7                | /*  0x0000F000 EXTI 7 configuration                                                                                   */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR2_EXTI4_PA             | /*  0x00000000 PA[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PB             | /*  0x00000001 PB[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PC             | /*  0x00000002 PC[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PD             | /*  0x00000003 PD[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PE             | /*  0x00000004 PE[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PF             | /*  0x00000005 PF[4] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI4_PG             | /*  0x00000006 PG[4] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR2_EXTI5_PA             | /*  0x00000000 PA[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PB             | /*  0x00000010 PB[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PC             | /*  0x00000020 PC[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PD             | /*  0x00000030 PD[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PE             | /*  0x00000040 PE[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PF             | /*  0x00000050 PF[5] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI5_PG             | /*  0x00000060 PG[5] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR2_EXTI6_PA             | /*  0x00000000 PA[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PB             | /*  0x00000100 PB[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PC             | /*  0x00000200 PC[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PD             | /*  0x00000300 PD[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PE             | /*  0x00000400 PE[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PF             | /*  0x00000500 PF[6] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI6_PG             | /*  0x00000600 PG[6] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR2_EXTI7_PA             | /*  0x00000000 PA[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PB             | /*  0x00001000 PB[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PC             | /*  0x00002000 PC[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PD             | /*  0x00003000 PD[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PE             | /*  0x00004000 PE[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PF             | /*  0x00005000 PF[7] pin                                                                                              */\
      0 * AFIO_EXTICR2_EXTI7_PG               /*  0x00006000 PG[7] pin                                                                                              */\
    )
    #if INIT_AFIO_EXTICR_1
      AFIO->EXTICR[1] = INIT_AFIO_EXTICR_1;
    #endif
    #undef INIT_AFIO_EXTICR_1
    
    #define INIT_AFIO_EXTICR_2   (          \
      0 * AFIO_EXTICR3_EXTI8                | /*  0x0000000F EXTI 8 configuration                                                                                   */\
      0 * AFIO_EXTICR3_EXTI9                | /*  0x000000F0 EXTI 9 configuration                                                                                   */\
      0 * AFIO_EXTICR3_EXTI10               | /*  0x00000F00 EXTI 10 configuration                                                                                  */\
      0 * AFIO_EXTICR3_EXTI11               | /*  0x0000F000 EXTI 11 configuration                                                                                  */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR3_EXTI8_PA             | /*  0x00000000 PA[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PB             | /*  0x00000001 PB[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PC             | /*  0x00000002 PC[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PD             | /*  0x00000003 PD[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PE             | /*  0x00000004 PE[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PF             | /*  0x00000005 PF[8] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI8_PG             | /*  0x00000006 PG[8] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR3_EXTI9_PA             | /*  0x00000000 PA[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PB             | /*  0x00000010 PB[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PC             | /*  0x00000020 PC[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PD             | /*  0x00000030 PD[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PE             | /*  0x00000040 PE[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PF             | /*  0x00000050 PF[9] pin                                                                                              */\
      0 * AFIO_EXTICR3_EXTI9_PG             | /*  0x00000060 PG[9] pin                                                                                              */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR3_EXTI10_PA            | /*  0x00000000 PA[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PB            | /*  0x00000100 PB[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PC            | /*  0x00000200 PC[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PD            | /*  0x00000300 PD[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PE            | /*  0x00000400 PE[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PF            | /*  0x00000500 PF[10] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI10_PG            | /*  0x00000600 PG[10] pin                                                                                             */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR3_EXTI11_PA            | /*  0x00000000 PA[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PB            | /*  0x00001000 PB[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PC            | /*  0x00002000 PC[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PD            | /*  0x00003000 PD[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PE            | /*  0x00004000 PE[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PF            | /*  0x00005000 PF[11] pin                                                                                             */\
      0 * AFIO_EXTICR3_EXTI11_PG              /*  0x00006000 PG[11] pin                                                                                             */\
    )
    #if INIT_AFIO_EXTICR_2
      AFIO->EXTICR[2] = INIT_AFIO_EXTICR_2;
    #endif
    #undef INIT_AFIO_EXTICR_2

    #define INIT_AFIO_EXTICR_3   (          \
      0 * AFIO_EXTICR4_EXTI12               | /*  0x0000000F EXTI 12 configuration                                                                                  */\
      0 * AFIO_EXTICR4_EXTI13               | /*  0x000000F0 EXTI 13 configuration                                                                                  */\
      0 * AFIO_EXTICR4_EXTI14               | /*  0x00000F00 EXTI 14 configuration                                                                                  */\
      0 * AFIO_EXTICR4_EXTI15               | /*  0x0000F000 EXTI 15 configuration                                                                                  */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR4_EXTI12_PA            | /*  0x00000000 PA[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PB            | /*  0x00000001 PB[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PC            | /*  0x00000002 PC[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PD            | /*  0x00000003 PD[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PE            | /*  0x00000004 PE[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PF            | /*  0x00000005 PF[12] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI12_PG            | /*  0x00000006 PG[12] pin                                                                                             */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR4_EXTI13_PA            | /*  0x00000000 PA[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PB            | /*  0x00000010 PB[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PC            | /*  0x00000020 PC[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PD            | /*  0x00000030 PD[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PE            | /*  0x00000040 PE[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PF            | /*  0x00000050 PF[13] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI13_PG            | /*  0x00000060 PG[13] pin                                                                                             */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR4_EXTI14_PA            | /*  0x00000000 PA[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PB            | /*  0x00000100 PB[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PC            | /*  0x00000200 PC[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PD            | /*  0x00000300 PD[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PE            | /*  0x00000400 PE[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PF            | /*  0x00000500 PF[14] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI14_PG            | /*  0x00000600 PG[14] pin                                                                                             */\
                                                                                                                                                                      \
      0 * AFIO_EXTICR4_EXTI15_PA            | /*  0x00000000 PA[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PB            | /*  0x00001000 PB[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PC            | /*  0x00002000 PC[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PD            | /*  0x00003000 PD[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PE            | /*  0x00004000 PE[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PF            | /*  0x00005000 PF[15] pin                                                                                             */\
      0 * AFIO_EXTICR4_EXTI15_PG              /*  0x00006000 PG[15] pin                                                                                             */\
    )
    #if INIT_AFIO_EXTICR_3
      AFIO->EXTICR[3] = INIT_AFIO_EXTICR_3;
    #endif
    #undef INIT_AFIO_EXTICR_3

    #define INIT_EXTI_IMR        (          \
      1 * EXTI_IMR_MR0                      | /*  0x00000001 Interrupt Mask on line 0                                                                               */\
      1 * EXTI_IMR_MR1                      | /*  0x00000002 Interrupt Mask on line 1                                                                               */\
      0 * EXTI_IMR_MR2                      | /*  0x00000004 Interrupt Mask on line 2                                                                               */\
      0 * EXTI_IMR_MR3                      | /*  0x00000008 Interrupt Mask on line 3                                                                               */\
      0 * EXTI_IMR_MR4                      | /*  0x00000010 Interrupt Mask on line 4                                                                               */\
      0 * EXTI_IMR_MR5                      | /*  0x00000020 Interrupt Mask on line 5                                                                               */\
      0 * EXTI_IMR_MR6                      | /*  0x00000040 Interrupt Mask on line 6                                                                               */\
      0 * EXTI_IMR_MR7                      | /*  0x00000080 Interrupt Mask on line 7                                                                               */\
      0 * EXTI_IMR_MR8                      | /*  0x00000100 Interrupt Mask on line 8                                                                               */\
      0 * EXTI_IMR_MR9                      | /*  0x00000200 Interrupt Mask on line 9                                                                               */\
      0 * EXTI_IMR_MR10                     | /*  0x00000400 Interrupt Mask on line 10                                                                              */\
      0 * EXTI_IMR_MR11                     | /*  0x00000800 Interrupt Mask on line 11                                                                              */\
      0 * EXTI_IMR_MR12                     | /*  0x00001000 Interrupt Mask on line 12                                                                              */\
      0 * EXTI_IMR_MR13                     | /*  0x00002000 Interrupt Mask on line 13                                                                              */\
      0 * EXTI_IMR_MR14                     | /*  0x00004000 Interrupt Mask on line 14                                                                              */\
      0 * EXTI_IMR_MR15                     | /*  0x00008000 Interrupt Mask on line 15                                                                              */\
      0 * EXTI_IMR_MR16                     | /*  0x00010000 Interrupt Mask on line 16                                                                              */\
      0 * EXTI_IMR_MR17                     | /*  0x00020000 Interrupt Mask on line 17                                                                              */\
      0 * EXTI_IMR_MR18                       /*  0x00040000 Interrupt Mask on line 18                                                                              */\
    )
    #if INIT_EXTI_IMR
      EXTI->IMR = INIT_EXTI_IMR;
    #endif
    #undef INIT_EXTI_IMR

    #define INIT_EXTI_RTSR       (          \
      0 * EXTI_RTSR_TR0                     | /*  0x00000001 Rising trigger event configuration bit of line 0                                                       */\
      0 * EXTI_RTSR_TR1                     | /*  0x00000002 Rising trigger event configuration bit of line 1                                                       */\
      0 * EXTI_RTSR_TR2                     | /*  0x00000004 Rising trigger event configuration bit of line 2                                                       */\
      0 * EXTI_RTSR_TR3                     | /*  0x00000008 Rising trigger event configuration bit of line 3                                                       */\
      0 * EXTI_RTSR_TR4                     | /*  0x00000010 Rising trigger event configuration bit of line 4                                                       */\
      0 * EXTI_RTSR_TR5                     | /*  0x00000020 Rising trigger event configuration bit of line 5                                                       */\
      0 * EXTI_RTSR_TR6                     | /*  0x00000040 Rising trigger event configuration bit of line 6                                                       */\
      0 * EXTI_RTSR_TR7                     | /*  0x00000080 Rising trigger event configuration bit of line 7                                                       */\
      0 * EXTI_RTSR_TR8                     | /*  0x00000100 Rising trigger event configuration bit of line 8                                                       */\
      0 * EXTI_RTSR_TR9                     | /*  0x00000200 Rising trigger event configuration bit of line 9                                                       */\
      0 * EXTI_RTSR_TR10                    | /*  0x00000400 Rising trigger event configuration bit of line 10                                                      */\
      0 * EXTI_RTSR_TR11                    | /*  0x00000800 Rising trigger event configuration bit of line 11                                                      */\
      0 * EXTI_RTSR_TR12                    | /*  0x00001000 Rising trigger event configuration bit of line 12                                                      */\
      0 * EXTI_RTSR_TR13                    | /*  0x00002000 Rising trigger event configuration bit of line 13                                                      */\
      0 * EXTI_RTSR_TR14                    | /*  0x00004000 Rising trigger event configuration bit of line 14                                                      */\
      0 * EXTI_RTSR_TR15                    | /*  0x00008000 Rising trigger event configuration bit of line 15                                                      */\
      0 * EXTI_RTSR_TR16                    | /*  0x00010000 Rising trigger event configuration bit of line 16                                                      */\
      0 * EXTI_RTSR_TR17                    | /*  0x00020000 Rising trigger event configuration bit of line 17                                                      */\
      0 * EXTI_RTSR_TR18                      /*  0x00040000 Rising trigger event configuration bit of line 18                                                      */\
    )
    #if INIT_EXTI_RTSR
      EXTI->RTSR = INIT_EXTI_RTSR;
    #endif
    #undef INIT_EXTI_RTSR

    #define INIT_EXTI_FTSR       (          \
      1 * EXTI_FTSR_TR0                     | /*  0x00000001 Falling trigger event configuration bit of line 0                                                      */\
      1 * EXTI_FTSR_TR1                     | /*  0x00000002 Falling trigger event configuration bit of line 1                                                      */\
      0 * EXTI_FTSR_TR2                     | /*  0x00000004 Falling trigger event configuration bit of line 2                                                      */\
      0 * EXTI_FTSR_TR3                     | /*  0x00000008 Falling trigger event configuration bit of line 3                                                      */\
      0 * EXTI_FTSR_TR4                     | /*  0x00000010 Falling trigger event configuration bit of line 4                                                      */\
      0 * EXTI_FTSR_TR5                     | /*  0x00000020 Falling trigger event configuration bit of line 5                                                      */\
      0 * EXTI_FTSR_TR6                     | /*  0x00000040 Falling trigger event configuration bit of line 6                                                      */\
      0 * EXTI_FTSR_TR7                     | /*  0x00000080 Falling trigger event configuration bit of line 7                                                      */\
      0 * EXTI_FTSR_TR8                     | /*  0x00000100 Falling trigger event configuration bit of line 8                                                      */\
      0 * EXTI_FTSR_TR9                     | /*  0x00000200 Falling trigger event configuration bit of line 9                                                      */\
      0 * EXTI_FTSR_TR10                    | /*  0x00000400 Falling trigger event configuration bit of line 10                                                     */\
      0 * EXTI_FTSR_TR11                    | /*  0x00000800 Falling trigger event configuration bit of line 11                                                     */\
      0 * EXTI_FTSR_TR12                    | /*  0x00001000 Falling trigger event configuration bit of line 12                                                     */\
      0 * EXTI_FTSR_TR13                    | /*  0x00002000 Falling trigger event configuration bit of line 13                                                     */\
      0 * EXTI_FTSR_TR14                    | /*  0x00004000 Falling trigger event configuration bit of line 14                                                     */\
      0 * EXTI_FTSR_TR15                    | /*  0x00008000 Falling trigger event configuration bit of line 15                                                     */\
      0 * EXTI_FTSR_TR16                    | /*  0x00010000 Falling trigger event configuration bit of line 16                                                     */\
      0 * EXTI_FTSR_TR17                    | /*  0x00020000 Falling trigger event configuration bit of line 17                                                     */\
      0 * EXTI_FTSR_TR18                      /*  0x00040000 Falling trigger event configuration bit of line 18                                                     */\
    )
    #if INIT_EXTI_FTSR
      EXTI->FTSR = INIT_EXTI_FTSR;
    #endif
    #undef INIT_EXTI_FTSR

    NVIC_SetPriority(EXTI0_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
    NVIC_EnableIRQ(EXTI0_IRQn);
    NVIC_SetPriority(EXTI1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
    NVIC_EnableIRQ(EXTI1_IRQn);
  #endif

  #if USE_TIM2

    #if TIM2_PSC
      TIM2->PSC  = TIM2_PSC - 1;
    #endif

    #if TIM2_ARR != 0xFFFF
      TIM2->ARR  = TIM2_ARR - 1;
    #endif

    #if TIM2_CCR1
      TIM2->CCR1 = TIM2_CCR1;
    #endif

    #if TIM2_CCR2
      TIM2->CCR2 = TIM2_CCR2;
    #endif

    #if TIM2_CCR3
      TIM2->CCR3 = TIM2_CCR3;
    #endif

    #if TIM2_CCR4
      TIM2->CCR4 = TIM2->CCR4;
    #endif

    TIM2->EGR = (
      1 * TIM_EGR_UG                        | /* 0x00000001 Update Generation                                                                                       */
      0 * TIM_EGR_CC1G                      | /* 0x00000002 Capture/Compare 1 Generation                                                                            */
      0 * TIM_EGR_CC2G                      | /* 0x00000004 Capture/Compare 2 Generation                                                                            */
      0 * TIM_EGR_CC3G                      | /* 0x00000008 Capture/Compare 3 Generation                                                                            */
      0 * TIM_EGR_CC4G                      | /* 0x00000010 Capture/Compare 4 Generation                                                                            */
      0 * TIM_EGR_COMG                      | /* 0x00000020 Capture/Compare Control Update Generation                                                               */
      0 * TIM_EGR_TG                        | /* 0x00000040 Trigger Generation                                                                                      */
      0 * TIM_EGR_BG                          /* 0x00000080 Break Generation                                                                                        */
    );

    TIM2->SR = 0;

    #define INIT_TIM2_CR2     (             \
      0 * TIM_CR2_CCPC                      | /*  0x00000001 Capture/Compare Preloaded Control                                                                      */\
      0 * TIM_CR2_CCUS                      | /*  0x00000004 Capture/Compare Control Update Selection                                                               */\
      0 * TIM_CR2_CCDS                      | /*  0x00000008 Capture/Compare DMA Selection                                                                          */\
                                                                                                                                                                      \
      0 * TIM_CR2_MMS                       | /*  0x00000070 MMS[2:0] bits (Master Mode Selection)                                                                  */\
      0 * TIM_CR2_MMS_0                     | /*    0x00000010                                                                                                      */\
      0 * TIM_CR2_MMS_1                     | /*    0x00000020                                                                                                      */\
      0 * TIM_CR2_MMS_2                     | /*    0x00000040                                                                                                      */\
                                                                                                                                                                      \
      0 * TIM_CR2_TI1S                      | /*  0x00000080 TI1 Selection                                                                                          */\
      0 * TIM_CR2_OIS1                      | /*  0x00000100 Output Idle state 1 (OC1 output)                                                                       */\
      0 * TIM_CR2_OIS1N                     | /*  0x00000200 Output Idle state 1 (OC1N output)                                                                      */\
      0 * TIM_CR2_OIS2                      | /*  0x00000400 Output Idle state 2 (OC2 output)                                                                       */\
      0 * TIM_CR2_OIS2N                     | /*  0x00000800 Output Idle state 2 (OC2N output)                                                                      */\
      0 * TIM_CR2_OIS3                      | /*  0x00001000 Output Idle state 3 (OC3 output)                                                                       */\
      0 * TIM_CR2_OIS3N                     | /*  0x00002000 Output Idle state 3 (OC3N output)                                                                      */\
      0 * TIM_CR2_OIS4                        /*  0x00004000 Output Idle state 4 (OC4 output)                                                                       */\
    )
    #if INIT_TIM2_CR2
      TIM2->CR2 = INIT_TIM2_CR2;
    #endif
    #undef INIT_TIM2_CR2

    TIM2->CCMR1 = (
      #if TIM2_CH1_ENABLE
        #if TIM2_CH1_CAPTURE_MODE
          0 * TIM_CCMR1_CC1S                | /*  0x00000003 CC1S[1:0] bits (Capture/Compare 1 Selection)                                                           */
          1 * TIM_CCMR1_CC1S_0              | /*   0x00000001                                                                                                       */
          0 * TIM_CCMR1_CC1S_1              | /*   0x00000002                                                                                                       */

          0 * TIM_CCMR1_IC1PSC              | /*  0x0000000C IC1PSC[1:0] bits (Input Capture 1 Prescaler)                                                           */
          0 * TIM_CCMR1_IC1PSC_0            | /*    0x00000004                                                                                                      */
          0 * TIM_CCMR1_IC1PSC_1            | /*    0x00000008                                                                                                      */

          0 * TIM_CCMR1_IC1F                | /*  0x000000F0 IC1F[3:0] bits (Input Capture 1 Filter)                                                                */
          0 * TIM_CCMR1_IC1F_0              | /*    0x00000010                                                                                                      */
          0 * TIM_CCMR1_IC1F_1              | /*    0x00000020                                                                                                      */
          0 * TIM_CCMR1_IC1F_2              | /*    0x00000040                                                                                                      */
          0 * TIM_CCMR1_IC1F_3              | /*    0x00000080                                                                                                      */
        #else
          0 * TIM_CCMR1_OC1FE               | /*  0x00000004 Output Compare 1 Fast enable                                                                           */
          0 * TIM_CCMR1_OC1PE               | /*  0x00000008 Output Compare 1 Preload enable                                                                        */

          0 * TIM_CCMR1_OC1M                | /*  0x00000070 OC1M[2:0] bits (Output Compare 1 Mode)                                                                 */
          1 * TIM_CCMR1_OC1M_0              | /*    0x00000010                                                                                                      */
          1 * TIM_CCMR1_OC1M_1              | /*    0x00000020                                                                                                      */
          1 * TIM_CCMR1_OC1M_2              | /*    0x00000040                                                                                                      */

          0 * TIM_CCMR1_OC1CE               | /*  0x00000080 Output Compare 1Clear Enable                                                                           */
        #endif
      #endif

      #if TIM2_CH2_ENABLE
        #if TIM2_CH2_CAPTURE_MODE
          0 * TIM_CCMR1_CC2S                | /*  0x00000300 CC2S[1:0] bits (Capture/Compare 2 Selection)                                                           */
          1 * TIM_CCMR1_CC2S_0              | /*    0x00000100                                                                                                      */
          0 * TIM_CCMR1_CC2S_1              | /*    0x00000200                                                                                                      */

          0 * TIM_CCMR1_IC2PSC              | /*  0x00000C00 IC2PSC[1:0] bits (Input Capture 2 Prescaler)                                                           */
          0 * TIM_CCMR1_IC2PSC_0            | /*    0x00000400                                                                                                      */
          0 * TIM_CCMR1_IC2PSC_1            | /*    0x00000800                                                                                                      */

          0 * TIM_CCMR1_IC2F                | /*  0x0000F000 IC2F[3:0] bits (Input Capture 2 Filter)                                                                */
          0 * TIM_CCMR1_IC2F_0              | /*    0x00001000                                                                                                      */
          0 * TIM_CCMR1_IC2F_1              | /*    0x00002000                                                                                                      */
          0 * TIM_CCMR1_IC2F_2              | /*    0x00004000                                                                                                      */
          0 * TIM_CCMR1_IC2F_3              | /*    0x00008000                                                                                                      */
        #else
          0 * TIM_CCMR1_OC2FE               | /*  0x00000400 Output Compare 2 Fast enable                                                                           */
          0 * TIM_CCMR1_OC2PE               | /*  0x00000800 Output Compare 2 Preload enable                                                                        */

          0 * TIM_CCMR1_OC2M                | /*  0x00007000 OC2M[2:0] bits (Output Compare 2 Mode)                                                                 */
          1 * TIM_CCMR1_OC2M_0              | /*    0x00001000                                                                                                      */
          1 * TIM_CCMR1_OC2M_1              | /*    0x00002000                                                                                                      */
          1 * TIM_CCMR1_OC2M_2              | /*    0x00004000                                                                                                      */

          0 * TIM_CCMR1_OC2CE               | /*  0x00008000 Output Compare 2 Clear Enable                                                                          */
        #endif
      #endif
      0
    );

    TIM2->CCMR2 = (
      #if TIM2_CH3_ENABLE
        #if TIM2_CH3_CAPTURE_MODE
          0 * TIM_CCMR2_CC3S                | /*  0x00000003 CC3S[1:0] bits (Capture/Compare 3 Selection)                                                           */
          1 * TIM_CCMR2_CC3S_0              | /*    0x00000001                                                                                                      */
          0 * TIM_CCMR2_CC3S_1              | /*    0x00000002                                                                                                      */

          0 * TIM_CCMR2_IC3PSC              | /*  0x0000000C IC3PSC[1:0] bits (Input Capture 3 Prescaler)                                                           */
          0 * TIM_CCMR2_IC3PSC_0            | /*    0x00000004                                                                                                      */
          0 * TIM_CCMR2_IC3PSC_1            | /*    0x00000008                                                                                                      */

          0 * TIM_CCMR2_IC3F                | /*  0x000000F0 IC3F[3:0] bits (Input Capture 3 Filter)                                                                */
          0 * TIM_CCMR2_IC3F_0              | /*    0x00000010                                                                                                      */
          0 * TIM_CCMR2_IC3F_1              | /*    0x00000020                                                                                                      */
          0 * TIM_CCMR2_IC3F_2              | /*    0x00000040                                                                                                      */
          0 * TIM_CCMR2_IC3F_3              | /*    0x00000080                                                                                                      */
        #else
          0 * TIM_CCMR2_OC3FE               | /*  0x00000004 Output Compare 3 Fast enable                                                                           */
          0 * TIM_CCMR2_OC3PE               | /*  0x00000008 Output Compare 3 Preload enable                                                                        */

          0 * TIM_CCMR2_OC3M                | /*  0x00000070 OC3M[2:0] bits (Output Compare 3 Mode)                                                                 */
          1 * TIM_CCMR2_OC3M_0              | /*    0x00000010                                                                                                      */
          1 * TIM_CCMR2_OC3M_1              | /*    0x00000020                                                                                                      */
          1 * TIM_CCMR2_OC3M_2              | /*    0x00000040                                                                                                      */

          0 * TIM_CCMR2_OC3CE               | /*  0x00000080 Output Compare 3 Clear Enable                                                                          */
        #endif
      #endif

      #if TIM2_CH4_ENABLE
        #if TIM2_CH4_CAPTURE_MODE
          0 * TIM_CCMR2_CC4S                | /*  0x00000300 CC4S[1:0] bits (Capture/Compare 4 Selection)                                                           */
          1 * TIM_CCMR2_CC4S_0              | /*    0x00000100                                                                                                      */
          0 * TIM_CCMR2_CC4S_1              | /*    0x00000200                                                                                                      */
          0 * TIM_CCMR2_IC4PSC              | /*  0x00000C00 IC4PSC[1:0] bits (Input Capture 4 Prescaler)                                                           */
          0 * TIM_CCMR2_IC4PSC_0            | /*    0x00000400                                                                                                      */
          0 * TIM_CCMR2_IC4PSC_1            | /*    0x00000800                                                                                                      */

          0 * TIM_CCMR2_IC4F                | /*  0x0000F000 IC4F[3:0] bits (Input Capture 4 Filter)                                                                */
          0 * TIM_CCMR2_IC4F_0              | /*    0x00001000                                                                                                      */
          0 * TIM_CCMR2_IC4F_1              | /*    0x00002000                                                                                                      */
          0 * TIM_CCMR2_IC4F_2              | /*    0x00004000                                                                                                      */
          0 * TIM_CCMR2_IC4F_3              |  /*    0x00008000                                                                                                      */
        #else
          0 * TIM_CCMR2_OC4FE               | /*  0x00000400 Output Compare 4 Fast enable                                                                           */
          0 * TIM_CCMR2_OC4PE               | /*  0x00000800 Output Compare 4 Preload enable                                                                        */

          0 * TIM_CCMR2_OC4M                | /*  0x00007000 OC4M[2:0] bits (Output Compare 4 Mode)                                                                 */
          1 * TIM_CCMR2_OC4M_0              | /*    0x00001000                                                                                                      */
          1 * TIM_CCMR2_OC4M_1              | /*    0x00002000                                                                                                      */
          1 * TIM_CCMR2_OC4M_2              | /*    0x00004000                                                                                                      */

          0 * TIM_CCMR2_OC4CE               | /*  0x00008000 Output Compare 4 Clear Enable                                                                          */
        #endif
      #endif
      0
    );

    TIM2->CCER = (

      #if TIM2_CH1_ENABLE && (TIM2_CH1_CAPTURE_MODE || TIM2_CH1_OUTPUT_ENABLE)
        TIM_CCER_CC1E                       | /*  0x00000001 Capture/Compare 1 output enable                                                                        */
      #endif
      #if TIM2_CH2_ENABLE && (TIM2_CH2_CAPTURE_MODE || TIM2_CH2_OUTPUT_ENABLE)
        TIM_CCER_CC2E                       | /*  0x00000010 Capture/Compare 2 output enable                                                                        */
      #endif
      #if TIM2_CH3_ENABLE && (TIM2_CH3_CAPTURE_MODE || TIM2_CH3_OUTPUT_ENABLE)
        TIM_CCER_CC3E                       | /*  0x00000100 Capture/Compare 3 output enable                                                                        */
      #endif
      #if TIM2_CH4_ENABLE && (TIM2_CH4_CAPTURE_MODE || TIM2_CH4_OUTPUT_ENABLE)
        TIM_CCER_CC4E                       | /*  0x00001000 Capture/Compare 4 output enable                                                                        */
      #endif

      0 * TIM_CCER_CC1P                     | /*  0x00000002 Capture/Compare 1 output Polarity                                                                      */
      0 * TIM_CCER_CC1NE                    | /*  0x00000004 Capture/Compare 1 Complementary output enable                                                          */
      0 * TIM_CCER_CC1NP                    | /*  0x00000008 Capture/Compare 1 Complementary output Polarity                                                        */
      0 * TIM_CCER_CC2P                     | /*  0x00000020 Capture/Compare 2 output Polarity                                                                      */
      0 * TIM_CCER_CC2NE                    | /*  0x00000040 Capture/Compare 2 Complementary output enable                                                          */
      0 * TIM_CCER_CC2NP                    | /*  0x00000080 Capture/Compare 2 Complementary output Polarity                                                        */
      0 * TIM_CCER_CC3P                     | /*  0x00000200 Capture/Compare 3 output Polarity                                                                      */
      0 * TIM_CCER_CC3NE                    | /*  0x00000400 Capture/Compare 3 Complementary output enable                                                          */
      0 * TIM_CCER_CC3NP                    | /*  0x00000800 Capture/Compare 3 Complementary output Polarity                                                        */
      0 * TIM_CCER_CC4P                       /*  0x00002000 Capture/Compare 4 output Polarity                                                                      */
    );

    TIM2->DIER = (
      0 * TIM_DIER_UIE                      | /*  0x00000001 Update interrupt enable                                                                                */
      0 * TIM_DIER_CC1IE                    | /*  0x00000002 Capture/Compare 1 interrupt enable                                                                     */
      0 * TIM_DIER_CC2IE                    | /*  0x00000004 Capture/Compare 2 interrupt enable                                                                     */
      0 * TIM_DIER_CC3IE                    | /*  0x00000008 Capture/Compare 3 interrupt enable                                                                     */
      1 * TIM_DIER_CC4IE                    | /*  0x00000010 Capture/Compare 4 interrupt enable                                                                     */
      0 * TIM_DIER_COMIE                    | /*  0x00000020 COM interrupt enable                                                                                   */
      0 * TIM_DIER_TIE                      | /*  0x00000040 Trigger interrupt enable                                                                               */
      0 * TIM_DIER_BIE                      | /*  0x00000080 Break interrupt enable                                                                                 */
      0 * TIM_DIER_UDE                      | /*  0x00000100 Update DMA request enable                                                                              */
      0 * TIM_DIER_CC1DE                    | /*  0x00000200 Capture/Compare 1 DMA request enable                                                                   */
      0 * TIM_DIER_CC2DE                    | /*  0x00000400 Capture/Compare 2 DMA request enable                                                                   */
      0 * TIM_DIER_CC3DE                    | /*  0x00000800 Capture/Compare 3 DMA request enable                                                                   */
      0 * TIM_DIER_CC4DE                    | /*  0x00001000 Capture/Compare 4 DMA request enable                                                                   */
      0 * TIM_DIER_COMDE                    | /*  0x00002000 COM DMA request enable                                                                                 */
      0 * TIM_DIER_TDE                        /*  0x00004000 Trigger DMA request enable                                                                             */
    );

    NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
    NVIC_EnableIRQ(TIM2_IRQn);

    TIM2->CR1 = (
      1 * TIM_CR1_CEN                       | /*  0x00000001 Counter enable                                                                                         */
      0 * TIM_CR1_UDIS                      | /*  0x00000002 Update disable                                                                                         */
      0 * TIM_CR1_URS                       | /*  0x00000004 Update request source                                                                                  */
      0 * TIM_CR1_OPM                       | /*  0x00000008 One pulse mode                                                                                         */
      0 * TIM_CR1_DIR                       | /*  0x00000010 Direction                                                                                              */

      0 * TIM_CR1_CMS                       | /*  0x00000060 CMS[1:0] bits (Center-aligned mode selection)                                                          */
      0 * TIM_CR1_CMS_0                     | /*    0x00000020                                                                                                      */
      0 * TIM_CR1_CMS_1                     | /*    0x00000040                                                                                                      */

      0 * TIM_CR1_ARPE                      | /*  0x00000080 Auto-reload preload enable                                                                             */

      0 * TIM_CR1_CKD                       | /*  0x00000300 CKD[1:0] bits (clock division)                                                                         */
      0 * TIM_CR1_CKD_0                     | /*    0x00000100                                                                                                      */
      0 * TIM_CR1_CKD_1                       /*    0x00000200                                                                                                      */
    );

  #endif /* USE_TIM2 */

  #if 1
    SystemCoreClockUpdate();
  #endif

  #if 1
    SysTick->LOAD = AHB_CLK / (8 KHZ) - 1;    /* set reload register T = 1 ms                                                                                       */
    SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;  /* start SysTick timer                                                                                                */
  #else
    SysTick->LOAD = AHB_CLK / (1 KHZ) - 1;    /* T = 1 ms                                                                                                           */
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
  #endif

  #if USE_ADC
    #define INIT_ADC1_CR1     (           \
      0 * ADC_CR1_AWDCH                   | /*  0x0000001F ADC analog watchdog 1 monitored channel selection                                                      */\
      0 * ADC_CR1_AWDCH_0                 | /*    0x00000001                                                                                                      */\
      0 * ADC_CR1_AWDCH_1                 | /*    0x00000002                                                                                                      */\
      0 * ADC_CR1_AWDCH_2                 | /*    0x00000004                                                                                                      */\
      0 * ADC_CR1_AWDCH_3                 | /*    0x00000008                                                                                                      */\
      0 * ADC_CR1_AWDCH_4                 | /*    0x00000010                                                                                                      */\
                                                                                                                                                                    \
      0 * ADC_CR1_EOSIE                   | /*  0x00000020 ADC group regular end of sequence conversions interrupt                                                */\
      0 * ADC_CR1_AWDIE                   | /*  0x00000040 ADC analog watchdog 1 interrupt                                                                        */\
      0 * ADC_CR1_JEOSIE                  | /*  0x00000080 ADC group injected end of sequence conversions interrupt                                               */\
      0 * ADC_CR1_SCAN                    | /*  0x00000100 ADC scan mode                                                                                          */\
      0 * ADC_CR1_AWDSGL                  | /*  0x00000200 ADC analog watchdog 1 monitoring a single channel or all channels                                      */\
      0 * ADC_CR1_JAUTO                   | /*  0x00000400 ADC group injected automatic trigger mode                                                              */\
      0 * ADC_CR1_DISCEN                  | /*  0x00000800 ADC group regular sequencer discontinuous mode                                                         */\
      0 * ADC_CR1_JDISCEN                 | /*  0x00001000 ADC group injected sequencer discontinuous mode                                                        */\
                                                                                                                                                                    \
      0 * ADC_CR1_DISCNUM                 | /*  0x0000E000 ADC group regular sequencer discontinuous number of ranks                                              */\
      0 * ADC_CR1_DISCNUM_0               | /*    0x00002000                                                                                                      */\
      0 * ADC_CR1_DISCNUM_1               | /*    0x00004000                                                                                                      */\
      0 * ADC_CR1_DISCNUM_2               | /*    0x00008000                                                                                                      */\
                                                                                                                                                                    \
      0 * ADC_CR1_DUALMOD                 | /*  0x000F0000 ADC multimode mode selection                                                                           */\
      0 * ADC_CR1_DUALMOD_0               | /*    0x00010000                                                                                                      */\
      0 * ADC_CR1_DUALMOD_1               | /*    0x00020000                                                                                                      */\
      0 * ADC_CR1_DUALMOD_2               | /*    0x00040000                                                                                                      */\
      0 * ADC_CR1_DUALMOD_3               | /*    0x00080000                                                                                                      */\
                                                                                                                                                                    \
      0 * ADC_CR1_JAWDEN                  | /*  0x00400000 ADC analog watchdog 1 enable on scope ADC group injected                                               */\
      0 * ADC_CR1_AWDEN                   | /*  0x00800000 ADC analog watchdog 1 enable on scope ADC group regular                                                */\
      0 * ADC_CR1_EOCIE                   | /*                                                                                                                    */\
      0 * ADC_CR1_JEOCIE                    /*                                                                                                                    */\
    )
    #if INIT_ADC1_CR1
      ADC1->CR1 = INIT_ADC1_CR1;  /* ADC control register 1. Address offset: 0x04, Reset value: 0x0000 0000                                                       */
    #endif
    #undef INIT_ADC1_CR1

    #define ADC_PARAM (/* ADC_CR2_CONT |*/ ADC_CR2_EXTSEL_0 | ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2 | ADC_CR2_TSVREFE | ADC_CR2_EXTTRIG)

    ADC1->CR2 = (  /* ADC control register 2. Address offset: 0x08, Reset value: 0x0000 0000                                                                        */
      1 * ADC_CR2_ADON                      | /*  0x00000001 ADC enable                                                                                             */
      0 * ADC_CR2_CONT                      | /*  0x00000002 ADC group regular continuous conversion mode                                                           */
      0 * ADC_CR2_CAL                       | /*  0x00000004 ADC calibration start                                                                                  */
      0 * ADC_CR2_RSTCAL                    | /*  0x00000008 ADC calibration reset                                                                                  */
      0 * ADC_CR2_DMA                       | /*  0x00000100 ADC DMA transfer enable                                                                                */
      0 * ADC_CR2_ALIGN                     | /*  0x00000800 ADC data alignement                                                                                    */

      0 * ADC_CR2_JEXTSEL                   | /*  0x00007000 ADC group injected external trigger source                                                             */
      0 * ADC_CR2_JEXTSEL_0                 | /*    0x00001000                                                                                                      */
      0 * ADC_CR2_JEXTSEL_1                 | /*    0x00002000                                                                                                      */
      0 * ADC_CR2_JEXTSEL_2                 | /*    0x00004000                                                                                                      */

      0 * ADC_CR2_JEXTTRIG                  | /*  0x00008000 ADC group injected external trigger enable                                                             */

      0 * ADC_CR2_EXTSEL                    | /*  0x000E0000 ADC group regular external trigger source                                                              */
      0 * ADC_CR2_EXTSEL_0                  | /*    0x00020000                                                                                                      */
      0 * ADC_CR2_EXTSEL_1                  | /*    0x00040000                                                                                                      */
      0 * ADC_CR2_EXTSEL_2                  | /*    0x00080000                                                                                                      */

      0 * ADC_CR2_EXTTRIG                   | /*  0x00100000 ADC group regular external trigger enable                                                              */
      0 * ADC_CR2_JSWSTART                  | /*  0x00200000 ADC group injected conversion start                                                                    */
      0 * ADC_CR2_SWSTART                   | /*  0x00400000 ADC group regular conversion start                                                                     */

      1 * ADC_CR2_TSVREFE                     /*  0x00800000 ADC internal path to VrefInt and temperature sensor enable                                             */
    );

    DELAY_MS(2);                              /* Before starting a calibration, the ADC must have been in power-on state for at least two ADC clock cycles.         */

    ADC1->CR2 |= ADC_CR2_CAL;

    while (ADC1->CR2 & ADC_CR2_CAL) {
      /* Wait until ADC calibration completes */
    }
                                                                                                                                                                    /*
      The Temperature sensor is connected to channel ADCx_IN16 and the internal reference
      voltage VREFINT is connected to ADCx_IN17. These two internal channels can be selected
      and converted as injected or regular channels.
                                                                                                                                                                    */
    #if 0 /* This block is for testing purposes only                                                                                                                */
      ADC1->SMPR1 = ADC_SMPR1(16, 239) | ADC_SMPR1(17, 239);
      ADC1->SQR3 = ADC_SQR3(1, 16);
      ADC1->SQR1 = ADC_SQR1_LEN(1);

      ADC1->CR2 |= ADC_CR2_ADON;
      while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
        /* Wait until conversion completes */
      }

      (void)ADC1->DR;

      ADC1->CR2 |= ADC_CR2_CONT;
      ADC1->CR2 |= ADC_CR2_ADON;

      enum {SAMPLES = 16};
      static uint32_t res1[SAMPLES], res2[SAMPLES];
      for(unsigned i = 0; i < SAMPLES; i++) {
        extract_randomness(&res1[i]);
      }

      ADC1->CR2 &= ~ADC_CR2_CONT;
      while ((ADC1->SR & ADC_SR_EOC) != ADC_SR_EOC) {
        /* Wait until conversion completes */
      }
      (void)ADC1->DR;

      ADC1->SQR3 = ADC_SQR3(1, 17);
      ADC1->CR2 |= ADC_CR2_CONT;
      ADC1->CR2 |= ADC_CR2_ADON;
      for(unsigned i = 0; i < SAMPLES; i++) {
        extract_randomness(&res2[i]);
      }
    #endif

    #if 1
      /* ADC1 interrupt Init */
      NVIC_SetPriority(ADC1_2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(ADC1_2_IRQn);
    #endif

  #endif /* USE_ADC */

  #if USE_I2C

    I2C1->CCR = I2C_CONFIGURE_CCR(APB1_CLK, I2C_SPEED_MODE); /* The CCR register must be configured only when the I2C is disabled (PE = 0)                          */
    I2C1->TRISE = I2C_RISE_TIME(APB1_CLK, I2C_SPEED_MODE);   /* TRISE[5:0]: Maximum rise time in Fm/Sm mode (Master mode)                                           */

    I2C1->CR2 = (
      1 * (APB1_CLK / 1000000)              | /*  FREQ[5:0] bits (Peripheral Clock Frequency)                                                                       */
      0 * I2C_CR2_ITERREN                   | /*  Error Interrupt Enable                                                                                            */
      0 * I2C_CR2_ITEVTEN                   | /*  Event Interrupt Enable                                                                                            */
      0 * I2C_CR2_ITBUFEN                   | /*  Buffer Interrupt Enable                                                                                           */
      0 * I2C_CR2_DMAEN                     | /*  DMA Requests Enable                                                                                               */
      0 * I2C_CR2_LAST                        /*  DMA Last Transfer                                                                                                 */
    );

    I2C1->CR1 = (
      1 * I2C_CR1_PE                        | /*  Peripheral Enable                                                                                                 */
      0 * I2C_CR1_SMBUS                     | /*  SMBus Mode                                                                                                        */
      0 * I2C_CR1_SMBTYPE                   | /*  SMBus Type                                                                                                        */
      0 * I2C_CR1_ENARP                     | /*  ARP Enable                                                                                                        */
      0 * I2C_CR1_ENPEC                     | /*  PEC Enable                                                                                                        */
      0 * I2C_CR1_ENGC                      | /*  General Call Enable                                                                                               */
      0 * I2C_CR1_NOSTRETCH                 | /*  Clock Stretching Disable (Slave mode)                                                                             */
      0 * I2C_CR1_START                     | /*  Start Generation                                                                                                  */
      0 * I2C_CR1_STOP                      | /*  Stop Generation                                                                                                   */
      0 * I2C_CR1_ACK                       | /*  Acknowledge Enable                                                                                                */
      0 * I2C_CR1_POS                       | /*  Acknowledge/PEC Position (for data reception)                                                                     */
      0 * I2C_CR1_PEC                       | /*  Packet Error Checking                                                                                             */
      0 * I2C_CR1_ALERT                     | /*  SMBus Alert                                                                                                       */
      0 * I2C_CR1_SWRST                       /*  Software Reset                                                                                                    */
    );

    #define INIT_DMA1_Channel6_CCR  (       \
      0 * DMA_CCR_EN                        | /*  0x00000001 Channel enable                                                                                         */\
      0 * DMA_CCR_TCIE                      | /*  0x00000002 Transfer complete interrupt enable                                                                     */\
      0 * DMA_CCR_HTIE                      | /*  0x00000004 Half Transfer interrupt enable                                                                         */\
      0 * DMA_CCR_TEIE                      | /*  0x00000008 Transfer error interrupt enable                                                                        */\
      0 * DMA_CCR_DIR                       | /*  0x00000010 Data transfer direction                                                                                */\
      0 * DMA_CCR_CIRC                      | /*  0x00000020 Circular mode                                                                                          */\
      0 * DMA_CCR_PINC                      | /*  0x00000040 Peripheral increment mode                                                                              */\
      0 * DMA_CCR_MINC                      | /*  0x00000080 Memory increment mode                                                                                  */\
      0 * DMA_CCR_PSIZE                     | /*  0x00000300 PSIZE[1:0] bits (Peripheral size)                                                                      */\
      0 * DMA_CCR_PSIZE_0                   | /*    0x00000100                                                                                                      */\
      0 * DMA_CCR_PSIZE_1                   | /*    0x00000200                                                                                                      */\
      0 * DMA_CCR_MSIZE                     | /*  0x00000C00 MSIZE[1:0] bits (Memory size)                                                                          */\
      0 * DMA_CCR_MSIZE_0                   | /*    0x00000400                                                                                                      */\
      0 * DMA_CCR_MSIZE_1                   | /*    0x00000800                                                                                                      */\
      0 * DMA_CCR_PL                        | /*  0x00003000 PL[1:0] bits(Channel Priority level)                                                                   */\
      0 * DMA_CCR_PL_0                      | /*    0x00001000                                                                                                      */\
      0 * DMA_CCR_PL_1                      | /*    0x00002000                                                                                                      */\
      0 * DMA_CCR_MEM2MEM                     /*  0x00004000 Memory to memory mode                                                                                  */\
    )
    #if INIT_DMA1_Channel6_CCR
      DMA1_Channel6->CCR = INIT_DMA1_Channel6_CCR; /* I2C1_TX                                                                                                       */
    #endif
    #undef INIT_DMA1_Channel6_CCR

    #define INIT_DMA1_Channel7_CCR  (       \
      0 * DMA_CCR_EN                        | /*  0x00000001 Channel enable                                                                                         */\
      0 * DMA_CCR_TCIE                      | /*  0x00000002 Transfer complete interrupt enable                                                                     */\
      0 * DMA_CCR_HTIE                      | /*  0x00000004 Half Transfer interrupt enable                                                                         */\
      0 * DMA_CCR_TEIE                      | /*  0x00000008 Transfer error interrupt enable                                                                        */\
      0 * DMA_CCR_DIR                       | /*  0x00000010 Data transfer direction                                                                                */\
      0 * DMA_CCR_CIRC                      | /*  0x00000020 Circular mode                                                                                          */\
      0 * DMA_CCR_PINC                      | /*  0x00000040 Peripheral increment mode                                                                              */\
      0 * DMA_CCR_MINC                      | /*  0x00000080 Memory increment mode                                                                                  */\
      0 * DMA_CCR_PSIZE                     | /*  0x00000300 PSIZE[1:0] bits (Peripheral size)                                                                      */\
      0 * DMA_CCR_PSIZE_0                   | /*    0x00000100                                                                                                      */\
      0 * DMA_CCR_PSIZE_1                   | /*    0x00000200                                                                                                      */\
      0 * DMA_CCR_MSIZE                     | /*  0x00000C00 MSIZE[1:0] bits (Memory size)                                                                          */\
      0 * DMA_CCR_MSIZE_0                   | /*    0x00000400                                                                                                      */\
      0 * DMA_CCR_MSIZE_1                   | /*    0x00000800                                                                                                      */\
      0 * DMA_CCR_PL                        | /*  0x00003000 PL[1:0] bits(Channel Priority level)                                                                   */\
      0 * DMA_CCR_PL_0                      | /*    0x00001000                                                                                                      */\
      0 * DMA_CCR_PL_1                      | /*    0x00002000                                                                                                      */\
      0 * DMA_CCR_MEM2MEM                     /*  0x00004000 Memory to memory mode                                                                                  */\
    )
    #if INIT_DMA1_Channel7_CCR
      DMA1_Channel7->CCR = INIT_DMA1_Channel7_CCR; /* I2C1_RX                                                                                                       */
    #endif
    #undef INIT_DMA1_Channel7_CCR

    #if 1
      /* I2C1 interrupt Init */
      NVIC_SetPriority(I2C1_EV_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(I2C1_EV_IRQn);
      NVIC_SetPriority(I2C1_ER_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(I2C1_ER_IRQn);
    #endif
  #endif /* USE_I2C */

  #if USE_SPI
    SPI1->CR1 = (
      0 * SPI_CR1_CPHA                      | /*  0x00000001 Clock Phase                                                                                            */
      0 * SPI_CR1_CPOL                      | /*  0x00000002 Clock Polarity                                                                                         */
      1 * SPI_CR1_MSTR                      | /*  0x00000004 Master Selection                                                                                       */

      0 * SPI_CR1_BR                        | /*  0x00000038 BR[2:0] bits (Baud Rate Control)
                                                    000: fPCLK/2
                                                    001: fPCLK/4
                                                    010: fPCLK/8
                                                    011: fPCLK/16
                                                    100: fPCLK/32
                                                    101: fPCLK/64
                                                    110: fPCLK/128
                                                    111: fPCLK/256
                                                    Note: These bits should not be changed when communication is ongoing.
                                                                                                                                                                    */
      1 * SPI_CR1_BR_0                      | /*    0x00000008                                                                                                      */
      0 * SPI_CR1_BR_1                      | /*    0x00000010                                                                                                      */
      0 * SPI_CR1_BR_2                      | /*    0x00000020                                                                                                      */


      1 * SPI_CR1_SPE                       | /*  0x00000040 SPI Enable                                                                                             */
      0 * SPI_CR1_LSBFIRST                  | /*  0x00000080 Frame Format                                                                                           */
      1 * SPI_CR1_SSI                       | /*  0x00000100 Internal slave select                                                                                  */
      1 * SPI_CR1_SSM                       | /*  0x00000200 Software slave management                                                                              */
      0 * SPI_CR1_RXONLY                    | /*  0x00000400 Receive only                                                                                           */
      0 * SPI_CR1_DFF                       | /*  0x00000800 Data Frame Format: 0 = 8-bit data, 1 = 16-bit                                                          */
      0 * SPI_CR1_CRCNEXT                   | /*  0x00001000 Transmit CRC next                                                                                      */
      0 * SPI_CR1_CRCEN                     | /*  0x00002000 Hardware CRC calculation enable                                                                        */
      0 * SPI_CR1_BIDIOE                    | /*  0x00004000 Output enable in bidirectional mode                                                                    */
      0 * SPI_CR1_BIDIMODE                    /*  0x00008000 Bidirectional data mode enable                                                                         */
    );

    #define INIT_SPI1_CR2       (           \
      0 * SPI_CR2_RXDMAEN                   | /*  0x00000001 Rx Buffer DMA Enable                                                                                   */\
      0 * SPI_CR2_TXDMAEN                   | /*  0x00000002 Tx Buffer DMA Enable                                                                                   */\
      0 * SPI_CR2_SSOE                      | /*  0x00000004 SS Output Enable                                                                                       */\
      0 * SPI_CR2_ERRIE                     | /*  0x00000020 Error Interrupt Enable                                                                                 */\
      0 * SPI_CR2_RXNEIE                    | /*  0x00000040 RX buffer Not Empty Interrupt Enable                                                                   */\
      0 * SPI_CR2_TXEIE                       /*  0x00000080 Tx buffer Empty Interrupt Enable                                                                       */\
    )
    #if INIT_SPI1_CR2
      SPI1->CR2 = INIT_SPI1_CR2;
    #endif
    #undef INIT_SPI1_CR2

    #define INIT_DMA1_Channel3_CCR   (      \
      0 * DMA_CCR_EN                        | /*  0x00000001 Channel enable                                                                                         */\
      0 * DMA_CCR_TCIE                      | /*  0x00000002 Transfer complete interrupt enable                                                                     */\
      0 * DMA_CCR_HTIE                      | /*  0x00000004 Half Transfer interrupt enable                                                                         */\
      0 * DMA_CCR_TEIE                      | /*  0x00000008 Transfer error interrupt enable                                                                        */\
      0 * DMA_CCR_DIR                       | /*  0x00000010 Data transfer direction                                                                                */\
      0 * DMA_CCR_CIRC                      | /*  0x00000020 Circular mode                                                                                          */\
      0 * DMA_CCR_PINC                      | /*  0x00000040 Peripheral increment mode                                                                              */\
      0 * DMA_CCR_MINC                      | /*  0x00000080 Memory increment mode                                                                                  */\
      0 * DMA_CCR_PSIZE                     | /*  0x00000300 PSIZE[1:0] bits (Peripheral size)                                                                      */\
      0 * DMA_CCR_PSIZE_0                   | /*    0x00000100                                                                                                      */\
      0 * DMA_CCR_PSIZE_1                   | /*    0x00000200                                                                                                      */\
      0 * DMA_CCR_MSIZE                     | /*  0x00000C00 MSIZE[1:0] bits (Memory size)                                                                          */\
      0 * DMA_CCR_MSIZE_0                   | /*    0x00000400                                                                                                      */\
      0 * DMA_CCR_MSIZE_1                   | /*    0x00000800                                                                                                      */\
      0 * DMA_CCR_PL                        | /*  0x00003000 PL[1:0] bits(Channel Priority level)                                                                   */\
      0 * DMA_CCR_PL_0                      | /*    0x00001000                                                                                                      */\
      0 * DMA_CCR_PL_1                      | /*    0x00002000                                                                                                      */\
      0 * DMA_CCR_MEM2MEM                     /*  0x00004000 Memory to memory mode                                                                                  */\
    )
    #if INIT_DMA1_Channel3_CCR
      DMA1_Channel3->CCR = INIT_DMA1_Channel3_CCR; /* SPI1_TX                                                                                                       */
    #endif
    #undef INIT_DMA1_Channel3_CCR

    #define INIT_DMA1_Channel2_CCR   (      \
      0 * DMA_CCR_EN                        | /*  0x00000001 Channel enable                                                                                         */\
      0 * DMA_CCR_TCIE                      | /*  0x00000002 Transfer complete interrupt enable                                                                     */\
      0 * DMA_CCR_HTIE                      | /*  0x00000004 Half Transfer interrupt enable                                                                         */\
      0 * DMA_CCR_TEIE                      | /*  0x00000008 Transfer error interrupt enable                                                                        */\
      0 * DMA_CCR_DIR                       | /*  0x00000010 Data transfer direction                                                                                */\
      0 * DMA_CCR_CIRC                      | /*  0x00000020 Circular mode                                                                                          */\
      0 * DMA_CCR_PINC                      | /*  0x00000040 Peripheral increment mode                                                                              */\
      0 * DMA_CCR_MINC                      | /*  0x00000080 Memory increment mode                                                                                  */\
      0 * DMA_CCR_PSIZE                     | /*  0x00000300 PSIZE[1:0] bits (Peripheral size)                                                                      */\
      0 * DMA_CCR_PSIZE_0                   | /*    0x00000100                                                                                                      */\
      0 * DMA_CCR_PSIZE_1                   | /*    0x00000200                                                                                                      */\
      0 * DMA_CCR_MSIZE                     | /*  0x00000C00 MSIZE[1:0] bits (Memory size)                                                                          */\
      0 * DMA_CCR_MSIZE_0                   | /*    0x00000400                                                                                                      */\
      0 * DMA_CCR_MSIZE_1                   | /*    0x00000800                                                                                                      */\
      0 * DMA_CCR_PL                        | /*  0x00003000 PL[1:0] bits(Channel Priority level)                                                                   */\
      0 * DMA_CCR_PL_0                      | /*    0x00001000                                                                                                      */\
      0 * DMA_CCR_PL_1                      | /*    0x00002000                                                                                                      */\
      0 * DMA_CCR_MEM2MEM                     /*  0x00004000 Memory to memory mode                                                                                  */\
    )
    #if INIT_DMA1_Channel2_CCR
      DMA1_Channel2->CCR = INIT_DMA1_Channel2_CCR; /* SPI1_RX                                                                                                       */
    #endif
    #undef INIT_DMA1_Channel2_CCR

    #if 1
      /* SPI1 interrupt Init */
      NVIC_SetPriority(SPI1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(SPI1_IRQn);
    #endif
  #endif /* USE_SPI */

  #if USE_UART

    #if UART_PORT_NO == 1
      USART_PORT->BRR = UART_BAUDRATE(APB2_CLK, USE_UART_BAUDRATE);
    #else
      UART_PORT->BRR = UART_BAUDRATE(APB1_CLK, USE_UART_BAUDRATE);
    #endif

    #if defined(USE_UART_DMA) && USE_UART_DMA
      UART_PORT->CR3 = (
        0 * USART_CR3_EIE                   | /*  Error Interrupt Enable                                                                                            */
        0 * USART_CR3_IREN                  | /*  IrDA mode Enable                                                                                                  */
        0 * USART_CR3_IRLP                  | /*  IrDA Low-Power                                                                                                    */
        0 * USART_CR3_HDSEL                 | /*  Half-Duplex Selection                                                                                             */
        0 * USART_CR3_NACK                  | /*  Smartcard NACK enable                                                                                             */
        0 * USART_CR3_SCEN                  | /*  Smartcard mode enable                                                                                             */
        0 * USART_CR3_DMAR                  | /*  DMA Enable Receiver                                                                                               */
        1 * USART_CR3_DMAT                  | /*  DMA Enable Transmitter                                                                                            */
        0 * USART_CR3_RTSE                  | /*  RTS Enable                                                                                                        */
        0 * USART_CR3_CTSE                  | /*  CTS Enable                                                                                                        */
        0 * USART_CR3_CTSIE                   /*  CTS Interrupt Enable                                                                                              */
      );
    #endif

    #if 0
      UART_PORT->CR2 = (
        0 * USART_CR2_ADD                   | /*  Address of the USART node                                                                                         */
        0 * USART_CR2_LBDL                  | /*  LIN Break Detection Length                                                                                        */
        0 * USART_CR2_LBDIE                 | /*  LIN Break Detection Interrupt Enable                                                                              */
        0 * USART_CR2_LBCL                  | /*  Last Bit Clock pulse                                                                                              */
        0 * USART_CR2_CPHA                  | /*  Clock Phase                                                                                                       */
        0 * USART_CR2_CPOL                  | /*  Clock Polarity                                                                                                    */
        0 * USART_CR2_CLKEN                 | /*  Clock Enable                                                                                                      */
        0 * USART_CR2_STOP                  | /*  STOP[1:0] bits (STOP bits)                                                                                        */
        0 * USART_CR2_STOP_0                | /*  Bit 0                                                                                                             */
        0 * USART_CR2_STOP_1                | /*  Bit 1                                                                                                             */
        0 * USART_CR2_LINEN                   /*  LIN mode enable                                                                                                   */
      );
    #endif

    UART_PORT->CR1 = (
      0 * USART_CR1_SBK                     | /*  Send Break                                                                                                        */
      0 * USART_CR1_RWU                     | /*  Receiver wakeup                                                                                                   */
      1 * USART_CR1_RE                      | /*  Receiver Enable                                                                                                   */
      1 * USART_CR1_TE                      | /*  Transmitter Enable                                                                                                */
      0 * USART_CR1_IDLEIE                  | /*  IDLE Interrupt Enable                                                                                             */
      0 * USART_CR1_RXNEIE                  | /*  RXNE Interrupt Enable                                                                                             */
      0 * USART_CR1_TCIE                    | /*  Transmission Complete Interrupt Enable                                                                            */
      0 * USART_CR1_TXEIE                   | /*  PE Interrupt Enable                                                                                               */
      0 * USART_CR1_PEIE                    | /*  PE Interrupt Enable                                                                                               */
      0 * USART_CR1_PS                      | /*  Parity Selection                                                                                                  */
      0 * USART_CR1_PCE                     | /*  Parity Control Enable                                                                                             */
      0 * USART_CR1_WAKE                    | /*  Wakeup method                                                                                                     */
      0 * USART_CR1_M                       | /*  Word length                                                                                                       */
      1 * USART_CR1_UE                        /*  USART Enable                                                                                                      */
    );

    while ((UART_PORT->SR & USART_SR_TC) == 0);
    UART_PORT->SR = ~USART_SR_TC;

    #if 1
      /* USARTx interrupt Init */
      #define IRQN _IRQn
      #define UART_IRQ CONCAT(UART_PORT_NO, IRQN)
      #define UART_IRQ_NO CONCAT(USART, UART_IRQ)
      NVIC_SetPriority(UART_IRQ_NO, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(UART_IRQ_NO);
    #endif
  #endif /* USE_UART */

  #if USE_RNG
    #define INIT_TIM4_DIER    (             \
      0 * TIM_DIER_UIE                      | /*  Update interrupt enable                                                                                           */\
      0 * TIM_DIER_CC1IE                    | /*  Capture/Compare 1 interrupt enable                                                                                */\
      0 * TIM_DIER_CC2IE                    | /*  Capture/Compare 2 interrupt enable                                                                                */\
      0 * TIM_DIER_CC3IE                    | /*  Capture/Compare 3 interrupt enable                                                                                */\
      0 * TIM_DIER_CC4IE                    | /*  Capture/Compare 4 interrupt enable                                                                                */\
      0 * TIM_DIER_COMIE                    | /*  COM interrupt enable                                                                                              */\
      0 * TIM_DIER_TIE                      | /*  Trigger interrupt enable                                                                                          */\
      0 * TIM_DIER_BIE                      | /*  Break interrupt enable                                                                                            */\
      0 * TIM_DIER_UDE                      | /*  Update DMA request enable                                                                                         */\
      0 * TIM_DIER_CC1DE                    | /*  Capture/Compare 1 DMA request enable                                                                              */\
      0 * TIM_DIER_CC2DE                    | /*  Capture/Compare 2 DMA request enable                                                                              */\
      0 * TIM_DIER_CC3DE                    | /*  Capture/Compare 3 DMA request enable                                                                              */\
      0 * TIM_DIER_CC4DE                    | /*  Capture/Compare 4 DMA request enable                                                                              */\
      0 * TIM_DIER_COMDE                    | /*  COM DMA request enable                                                                                            */\
      0 * TIM_DIER_TDE                        /*  Trigger DMA request enable                                                                                        */\
    )
    #if INIT_TIM4_DIER
      TIM4->DIER = INIT_TIM4_DIER;
    #endif
    #undef INIT_TIM4_DIER

    #if (defined(USE_CAPTURE) && USE_CAPTURE != 0)
      TIM4->CCMR1 = (
        0 * TIM_CCMR1_CC1S                  | /*  CC1S[1:0] bits (Capture/Compare 1 Selection)                                                                      */
        1 * TIM_CCMR1_CC1S_0                | /*    0x00000001                                                                                                      */
        0 * TIM_CCMR1_CC1S_1                | /*    0x00000002                                                                                                      */

        0 * TIM_CCMR1_CC2S                  | /*  CC2S[1:0] bits (Capture/Compare 2 Selection)                                                                      */
        1 * TIM_CCMR1_CC2S_0                | /*    0x00000100                                                                                                      */
        0 * TIM_CCMR1_CC2S_1                  /*    0x00000200                                                                                                      */
      );

      TIM4->CCER = (
        1 * TIM_CCER_CC1E                   | /*  Capture/Compare 1 output enable                                                                                   */
        0 * TIM_CCER_CC1P                   | /*  Capture/Compare 1 output Polarity                                                                                 */
        1 * TIM_CCER_CC2E                   | /*  Capture/Compare 2 output enable                                                                                   */
        0 * TIM_CCER_CC2P                     /*  Capture/Compare 2 output Polarity                                                                                 */
      );
    #endif

    #if 0
      /* TIM4 interrupt Init */
      NVIC_SetPriority(TIM4_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(TIM4_IRQn);
    #endif
    
    #define INIT_TIM4_CR1       (           \
      0 * TIM_CR1_CEN                       | /*  Counter enable                                                                                                    */\
      0 * TIM_CR1_UDIS                      | /*  Update disable                                                                                                    */\
      0 * TIM_CR1_URS                       | /*  Update request source                                                                                             */\
      0 * TIM_CR1_OPM                       | /*  One pulse mode                                                                                                    */\
      0 * TIM_CR1_DIR                       | /*  Direction                                                                                                         */\
      0 * TIM_CR1_CMS                       | /*  CMS[1:0] bits (Center-aligned mode selection)                                                                     */\
      0 * TIM_CR1_CMS_0                     | /*  Bit 0                                                                                                             */\
      0 * TIM_CR1_CMS_1                     | /*  Bit 1                                                                                                             */\
      0 * TIM_CR1_ARPE                      | /*  Auto-reload preload enable                                                                                        */\
      0 * TIM_CR1_CKD                       | /*  CKD[1:0] bits (clock division)                                                                                    */\
      0 * TIM_CR1_CKD_0                     | /*  Bit 0                                                                                                             */\
      0 * TIM_CR1_CKD_1                       /*  Bit 1                                                                                                             */\
    )
    #if INIT_TIM4_CR1
      TIM4->CR1 = INIT_TIM4_CR1;
    #endif
    #undef INIT_TIM4_CR1

    for (unsigned i = 0; i < 1024; i++) {
      RCC->CSR = RCC_CSR_LSION;               /*  Enable Internal Low Speed oscillator                                                                              */
      while((RCC->CSR & RCC_CSR_LSIRDY) != RCC_CSR_LSIRDY) {
        /* wait until LSI oscillations become stable */
      }

      RCC->CSR = 0;                           /*  Disable Internal Low Speed oscillator                                                                             */
      while((RCC->CSR & RCC_CSR_LSIRDY) == RCC_CSR_LSIRDY) {
        /* wait until LSI oscillator shuts down */
      }
    }

    RCC->CSR = RCC_CSR_LSION;               /*  Enable Internal Low Speed oscillator                                                                              */

    extern volatile uint32_t t4_irq_cnt;
    extern void srand(unsigned int);
    uint32_t rnd_cnt = TIM4->CNT;
    rnd_cnt += t4_irq_cnt * 65536;
    srand(__REV(rnd_cnt) ^ ((uint32_t*)UID_BASE)[0] ^ ((uint32_t*)UID_BASE)[1] ^ ((uint32_t*)UID_BASE)[2]);

    RCC->APB1RSTR = RCC_APB1RSTR_TIM4RST;
    __NOP();__NOP();__NOP();__NOP();__NOP();__NOP();
    RCC->APB1RSTR = 0;

    #if !defined(USE_TIM4) || USE_TIM4 == 0
      // RCC->APB1ENR &= ~RCC_APB1ENR_TIM4EN;
    #endif

  #endif /* USE_RNG */


  #if USE_RTC
    #if 0
      PWR->CR = (
        0 * PWR_CR_LPDS                     | /*  0x00000001 Low-Power Deepsleep                                                                                    */
        0 * PWR_CR_PDDS                     | /*  0x00000002 Power Down Deepsleep                                                                                   */
        0 * PWR_CR_CWUF                     | /*  0x00000004 Clear Wakeup Flag                                                                                      */
        0 * PWR_CR_CSBF                     | /*  0x00000008 Clear Standby Flag                                                                                     */
        0 * PWR_CR_PVDE                     | /*  0x00000010 Power Voltage Detector Enable                                                                          */

        0 * PWR_CR_PLS                      | /*  0x000000E0 PLS[2:0] bits (PVD Level Selection)                                                                    */
        0 * PWR_CR_PLS_0                    | /*    0x00000020                                                                                                      */
        0 * PWR_CR_PLS_1                    | /*    0x00000040                                                                                                      */
        0 * PWR_CR_PLS_2                    | /*    0x00000080                                                                                                      */

        0 * PWR_CR_PLS_LEV0                 | /*    PVD level 2.2V                                                                                                  */
        0 * PWR_CR_PLS_LEV1                 | /*    PVD level 2.3V                                                                                                  */
        0 * PWR_CR_PLS_LEV2                 | /*    PVD level 2.4V                                                                                                  */
        0 * PWR_CR_PLS_LEV3                 | /*    PVD level 2.5V                                                                                                  */
        0 * PWR_CR_PLS_LEV4                 | /*    PVD level 2.6V                                                                                                  */
        0 * PWR_CR_PLS_LEV5                 | /*    PVD level 2.7V                                                                                                  */
        0 * PWR_CR_PLS_LEV6                 | /*    PVD level 2.8V                                                                                                  */
        0 * PWR_CR_PLS_LEV7                 | /*    PVD level 2.9V                                                                                                  */

        0 * PWR_CR_PLS_2V2                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V3                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V4                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V5                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V6                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V7                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V8                  | /*                                                                                                                    */
        0 * PWR_CR_PLS_2V9                  | /*                                                                                                                    */

        1 * PWR_CR_DBP                        /*  0x00000100 Disable Backup Domain write protection                                                                 */
      );
    #endif

    RCC->BDCR = RCC_BDCR_BDRST;               /*  Perform Backup domain software reset                                                                              */

    #if defined(USE_HSE) && USE_HSE
      while(!(RCC->CR & RCC_CR_HSERDY)) {}    /*  Wait for External High Speed clock ready flag                                                                     */
    #endif

    DELAY_MS(2);

    RCC->BDCR = (
      USE_LSE * RCC_BDCR_LSEON              | /*  0x00000001 External Low Speed oscillator enable                                                                   */
      0 * RCC_BDCR_LSERDY                   | /*  0x00000002 External Low Speed oscillator Ready                                                                    */
      0 * RCC_BDCR_LSEBYP                   | /*  0x00000004 External Low Speed oscillator Bypass                                                                   */

      0 * RCC_BDCR_RTCSEL                   | /*  0x00000300 RTCSEL[1:0] bits (RTC clock source selection)                                                          */
      0 * RCC_BDCR_RTCSEL_0                 | /*    0x00000100                                                                                                      */
      0 * RCC_BDCR_RTCSEL_1                 | /*    0x00000200                                                                                                      */
      0 * RCC_BDCR_RTCSEL_NOCLOCK           | /*  No clock                                                                                                          */

      #if USE_LSE
        1 * RCC_BDCR_RTCSEL_LSE             | /*  LSE oscillator clock used as RTC clock                                                                            */
      #elif USE_HSE
        1 * RCC_BDCR_RTCSEL_HSE             | /*  HSE oscillator clock divided by 128 used as RTC clock                                                             */
      #else
        1 * RCC_BDCR_RTCSEL_LSI             | /*  LSI oscillator clock used as RTC clock                                                                            */
      #endif

      1 * RCC_BDCR_RTCEN                    | /*  0x00008000 RTC clock enable                                                                                       */
      0 * RCC_BDCR_BDRST                      /*  0x00010000 Backup domain software reset                                                                           */
    );

    #if 0
      BKP->RTCCR |= BKP_RTCCR_CCO;            /* Enable RTC clock with a frequency divided by 64 on the TAMPER pin.                                                 */
    #endif

    #if 0
    while((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) {
      /* wait until LSE oscillations become stable */
    }
    #endif
                                                                                                                                                                    /*
      Configuration procedure
      =======================
      1. Poll RTOFF, wait until its value goes to �1�
      2. Set the CNF bit to enter configuration mode
      3. Write to one or more RTC registers
      4. Clear the CNF bit to exit configuration mode
      5. Poll RTOFF, wait until its value goes to �1� to check the end of the write operation.

      The write operation only executes when the CNF bit is cleared; it takes at least three
      RTCCLK cycles to complete.                                                                                                                                    */

    /* 1. */
    while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF) {
      /* wait until the last write operation completes */
    }

    /* 2. */
    RTC->CRL = RTC_CRL_CNF;

    #if USE_LSE
      /* 3. */
                                                                                                                                                                    /*
      Note: If the input clock frequency (fRTCCLK) is 32.768 kHz, write 7FFFh in this register to get a
            signal period of 1 second.
                                                                                                                                                                    */
      RTC->PRLH = 0;
      RTC->PRLL = 0x7FFF;
    #elif USE_HSE
      RTC->PRLH = 0;
      RTC->PRLL = 0xF423;
    #else
      RTC->PRLH = 0;
      RTC->PRLL = 0x9CC0; // 0x9C3F
    #endif

    /* Example: 10.10.2020 10:10:10 = 1602324610 = 0x5F818882 */

    time_struct_t t = {.year = 2020, .month = 11, .day = 17, .hour = 7, .minute = 31, .second = 40 };

    uint32_t cnt = time_to_unixtime(&t);

    #if 0
      RTC->CNTH = 0x5F81;                     /* Set RTC COUNTER MSB word                                                                                           */
      RTC->CNTL = 0x8882;                     /* Set RTC COUNTER LSB word                                                                                           */
    #else
      RTC->CNTH = cnt >> 16;                  /* Set RTC COUNTER MSB word                                                                                           */
      RTC->CNTL = cnt & 0xFFFF;               /* Set RTC COUNTER LSB word                                                                                           */
    #endif

    #if USE_RTC_ALARM
      cnt += 25;

      RTC->ALRH = cnt >> 16;                  /* Set RTC Alarm Register MSB word                                                                                    */
      RTC->ALRL = cnt & 0xFFFF;               /* Set RTC Alarm Register LSB word                                                                                    */
    #endif

    RTC->CRH = (
      USE_RTC_ALARM_IRQ * RTC_CRH_ALRIE   |
      USE_RTC_SEC_IRQ   * RTC_CRH_SECIE
    );

    /* 4. */
    RTC->CRL = 0;                           /* The write operation is only executed when the CNF bit is reset by software after has been set.                       */

    /* 5. */
    while((RTC->CRL & RTC_CRL_RTOFF) != RTC_CRL_RTOFF) {
      /* wait until the last write operation completes */
    }

    /* *** Note: If the HSE divided by 128 is used as the RTC clock, this bit must remain set to 1.                                                             *** */
    #if USE_LSE || !USE_HSE
      PWR->CR = 0;                          /* Restore Backup Domain write protection                                                                               */
    #endif

    //PWR->CR = 0;                          /* Restore Backup Domain write protection                                                                               */

    #if (USE_RTC_SEC_IRQ != 0) || (USE_RTC_ALARM_IRQ != 0)
      NVIC_SetPriority(RTC_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_EnableIRQ(RTC_IRQn);
    #endif

    #if USE_RTC_ALARM_EXTI_IRQ != 0

      /* EXTI line 17 is connected to the RTC Alarm event  */

      /* Configure exti and nvic for RTC IT                */
      /* (13) unmask line 17                               */
      /* (14) Rising edge for line 17                      */
      /* (15) Clear pending request line 17                */

      /* (16) Set priority                                 */
      /* (17) Enable RTC_IRQn                              */


      EXTI->IMR  |= EXTI_IMR_MR17;  /* (13) */
      EXTI->RTSR |= EXTI_RTSR_TR17; /* (14) */
      EXTI->PR   |= EXTI_PR_PR17;

      NVIC_SetPriority(RTC_Alarm_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
      NVIC_ClearPendingIRQ(RTC_Alarm_IRQn);
      NVIC_EnableIRQ(RTC_Alarm_IRQn);           /* Enable RTC Alarm through EXTI Line Interrupt                                                                     */
    #endif

  #endif /* USE_RTC */

  #if USE_IWDG
    RCC->CSR = RCC_CSR_LSION;                 /*  Enable Internal Low Speed oscillator                                                                              */
    while((RCC->CSR & RCC_CSR_LSIRDY) != RCC_CSR_LSIRDY) {
      /* wait until LSI oscillations become stable */
    }
    IWDG->KR = 0x5555;
                                                                                                                                                                    /*
      Min/max IWDG timeout period (in ms) at 40 kHz (LSI)(1)
    =======================================================================
      Prescaler      PR[2:0] bits       Min timeout         Max timeout
       divider                        RL[11:0]= 0x000     RL[11:0]= 0xFFF

          /4              0                 0.1                409.6
          /8              1                 0.2                819.2
         /16              2                 0.4               1638.4
         /32              3                 0.8               3276.8
         /64              4                 1.6               6553.6
        /128              5                 3.2              13107.2
        /256              6 (or 7)          6.4              26214.4
    =======================================================================                                                                                         */

    IWDG->PR = 2;
    IWDG->RLR = 0xFFF;

    IWDG->KR = 0xCCCC;                        /*  Start the watchdog                                                                                                */
  #endif /* USE_IWDG */
}

#if 0
  __STATIC_FORCEINLINE void delay_ms(uint32_t ms) {
    (void) SysTick->CTRL;
    while(ms -= (SysTick->CTRL >> SysTick_CTRL_COUNTFLAG_Pos)) {
      /* Wait until t becomes zerro */
    }
  }
#endif

#if USE_CRC
  __STATIC_FORCEINLINE unsigned crc32_zlib(const unsigned *data, unsigned short cnt) {

    unsigned i;

    CRC->CR = CRC_CR_RESET;

    for (i = 0; i < (cnt / 4); i++) CRC->DR = __RBIT(data[i]);

    unsigned result = __RBIT(CRC->DR);
    cnt = (cnt % 4) * 8;
    if (cnt) {
      CRC->DR = CRC->DR;
      CRC->DR = __RBIT((data[i] & (0xFFFFFFFF >> (32 - cnt))) ^ result) >> (32 - cnt);
      result = (result >> cnt) ^ __RBIT(CRC->DR);
    }
    return ~result;
  }
#endif /* USE_CRC */

#if USE_I2C
  #define I2C_TIMEOUT      10
  #define I2C_ERR_TIMEOUT  1
  #define I2C_ERROR        (                                                     \
                             I2C_SR1_BERR | /*  Bus Error           0x00000100 */\
                             I2C_SR1_ARLO | /*  Arbitration Lost    0x00000200 */\
                             I2C_SR1_AF   | /*  Acknowledge Failure 0x00000400 */\
                             I2C_SR1_OVR    /*  Overrun/Underrun    0x00000800 */\
                           )

  extern unsigned short i2c_status;

  __STATIC_INLINE ErrorStatus i2c_check(const unsigned bitFlag) {
    unsigned t_out = I2C_TIMEOUT;
    (void) SysTick->CTRL;

    while((I2C1->SR1 & bitFlag) == 0) {
      t_out -= SysTick->CTRL >> SysTick_CTRL_COUNTFLAG_Pos;
      if (t_out == 0) {
        i2c_status |= I2C_ERR_TIMEOUT;
        return ERROR;
      }

      unsigned e_status = I2C1->SR1 & I2C_ERROR;
      if (e_status) {
        i2c_status = (unsigned short)e_status;
        return ERROR;
      }
    }

    return SUCCESS;
  }

  __STATIC_INLINE ErrorStatus i2c_write(const char addr, const char *data, unsigned len) {
    I2C1->CR1 = I2C_CR1_START | I2C_CR1_ACK | I2C_CR1_PE;
    if (i2c_check(I2C_SR1_SB) == ERROR) return ERROR;

    I2C1->DR = (uint32_t) addr << 1;
    if (i2c_check(I2C_SR1_ADDR) == ERROR) return ERROR;

    (void) I2C1->SR2;

    for(unsigned i = 0; i < len; i++) {
      if (i2c_check(I2C_SR1_TXE) == ERROR) return ERROR;
      I2C1->DR = data[i];
    }

    if (i2c_check(I2C_SR1_BTF) == ERROR) return ERROR;

    I2C1->CR1 = I2C_CR1_STOP | I2C_CR1_ACK | I2C_CR1_PE;
    return (ErrorStatus)(I2C1->SR1 & I2C_ERROR);
  }

  __STATIC_INLINE ErrorStatus i2c_read(const char addr, volatile char *data, unsigned len) {

    I2C1->CR1 = I2C_CR1_START | I2C_CR1_ACK | I2C_CR1_PE;
    if (i2c_check(I2C_SR1_SB) == ERROR) return ERROR;

    I2C1->DR = (char)(addr << 1) + 1;
    if (i2c_check(I2C_SR1_ADDR) == ERROR) return ERROR;
    if (len == 1) I2C1->CR1 = I2C_CR1_PE;                 /* reset ACK bit              */

    (void) I2C1->SR2;

    for(unsigned i = 0; i < len; i++) {
      if (i == len - 1) I2C1->CR1 = I2C_CR1_PE;           /* reset ACK bit              */
      if (i2c_check(I2C_SR1_RXNE) == ERROR) return ERROR;
      data[i] = (char) I2C1->DR;
    }

    if (i2c_check(I2C_SR1_BTF) == ERROR) return ERROR;
    I2C1->CR1 = I2C_CR1_STOP | I2C_CR1_ACK | I2C_CR1_PE;

    return (ErrorStatus)(I2C1->SR1 & I2C_ERROR);
  }

  __STATIC_INLINE ErrorStatus i2c_read_reg(const char addr, char *data, unsigned reg, unsigned len) {
    I2C1->CR1 = I2C_CR1_START | I2C_CR1_ACK | I2C_CR1_PE; /* generate START             */
    if (i2c_check(I2C_SR1_SB) == ERROR) return ERROR;

    I2C1->DR = (unsigned) addr << 1;                      /* send slave address         */
    if (i2c_check(I2C_SR1_ADDR) == ERROR) return ERROR;

    (void) I2C1->SR2;                                     /* dummy read status register */
    if (i2c_check(I2C_SR1_TXE) == ERROR) return ERROR;

    I2C1->DR = reg;                                       /* send register number       */
    if (i2c_check(I2C_SR1_BTF) == ERROR) return ERROR;

    I2C1->CR1 = I2C_CR1_START | I2C_CR1_ACK | I2C_CR1_PE; /* generate RE-START          */
    if (i2c_check(I2C_SR1_SB) == ERROR) return ERROR;

    I2C1->DR = (char)(addr << 1) + 1;                     /* send slave addr + read req */
    if (i2c_check(I2C_SR1_ADDR) == ERROR) return ERROR;

    if (len == 1) I2C1->CR1 = I2C_CR1_PE;                 /* reset ACK bit              */

    (void) I2C1->SR2;                                     /* dummy read status register */

    for(unsigned i = 0; i < len; i++) {
      if (i == len - 1) I2C1->CR1 = I2C_CR1_PE;           /* reset ACK bit              */
      if (i2c_check(I2C_SR1_RXNE) == ERROR) return ERROR;
      data[i] = (uint8_t) I2C1->DR;                       /* store received data        */
    }

    if (i2c_check(I2C_SR1_BTF) == ERROR) return ERROR;
    I2C1->CR1 = I2C_CR1_STOP | I2C_CR1_ACK | I2C_CR1_PE;  /* generate STOP              */

    return (ErrorStatus)(I2C1->SR1 & I2C_ERROR);
  }
#endif /* USE_I2C */


__STATIC_INLINE unsigned rtc_read_counter(void) {

  #if 0
    unsigned a, b, c, d;
    do {
      a = RTC->CNTH;
      b = RTC->CNTL;
      c = RTC->CNTH;
      d = RTC->CNTL;
    } while ((a != c) || (b != d));

    a = (a << 16) | b;
    return a;
  #else
    unsigned a, b, c;
    a = RTC->CNTH;
    b = RTC->CNTL;
    c = RTC->CNTH;

    if (a != c) {           /* In this case the counter roll over during reading of CNTL and CNTH registers, read again CNTL register then return the counter value */
      a = (c << 16U) | RTC->CNTL;
    } else {                /* No counter roll over during reading of CNTL and CNTH registers, counter value is equal to first value of CNTL and CNTH               */
      a = (a << 16U) | b;
    }
    return a;
  #endif
}

__STATIC_INLINE uint32_t tripple_blink(void) {

  if (RCC->BDCR & RCC_BDCR_RTCEN) {
    if ((RTC->CRL & RTC_CRL_SECF) != RTC_CRL_SECF){
      return 0;
    }
    RTC->CRL &= ~RTC_CRL_SECF;
  } else {
    DELAY_MS(820);
  }

  #if USE_IWDG
    IWDG->KR = 0xAAAA;
  #endif

  for(unsigned i = 0; i < 3; i++) {
    _SW(C, 13, LOW);
    DELAY_MS(30);
    _SW(C, 13, HIGH);
    DELAY_MS(30);
  }
  // _SW(C, 13, LOW);
  // DELAY_MS(30);
  // _SW(C, 13, HIGH);
  // DELAY_MS(30);
  //
  // _SW(C, 13, LOW);
  // DELAY_MS(30);
  // _SW(C, 13, HIGH);
  // DELAY_MS(30);

  return !0;
}

// #ifdef __GNUC__
//   #pragma GCC diagnostic push
//   #pragma GCC diagnostic ignored "-Wunused-value"
// #endif
//
// #ifdef __GNUC__
//   #pragma GCC diagnostic pop
// #endif

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

